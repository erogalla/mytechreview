# All the contents are copied from the sample
.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# All of these make commands must be called in root directory

build-backend:
	docker build -t backend ./back-end

# run docker
# docker run -p 3000:3000 -it danielhcai/mytechreview
run-backend:
	docker run -p 8000:8000 backend

build-frontend:
	docker build -t frontend ./front-end

run-frontend:
	docker run -it -p 3000:3000 -v $(shell pwd)/front-end:/app frontend

# run docker-compose
docker-compose:
	docker-compose up --build --force-recreate

all:

# auto format the code
format:
	black ./back-end/*.py

install:
	pip install -r ./back-end/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        


# uncomment the following line once you've pushed your test files
# you must replace GitLabID with your GitLabID

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__
	rm server.pid
