import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import sys

# TO RUN:
# python ./front-end/selenium_tests/SplashTests.py

# Inspired by CulturedFoodies: https://gitlab.com/cs373-group-11/cultured-foodies/-/blob/master/frontend/src/tests/selenium_tests.py

URL = 'https://dev.mytechreview.me/#/'
PATH = "./selenium_tests/chromedriver.exe"

class Selenium_Tests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        # chrome_options.add_argument('--disable-dev-shm-usage')
        # service = Service('./chromedriver.exe')
        # PATH = "./selenium_tests/chromedriver.exe"
        self.driver = webdriver.Chrome(executable_path=PATH, options=chrome_options) #webdriver.Chrome(options=chrome_options, service=service)
        self.driver.get(URL)
        self.driver.implicitly_wait(20)
    
    def tearDown(self):
        self.driver.quit()

    def testTitle(self):
        assert self.driver.title == "My Tech Review"
    
    def testProductCard(self):
        self.driver.find_element(By.ID, 'products').click()
        assert 'https://dev.mytechreview.me/#/products' in self.driver.current_url
        self.driver.back()
        assert self.driver.current_url == URL
    
    def testBrandCard(self):
        self.driver.find_element(By.ID, 'brands').click()
        assert 'https://dev.mytechreview.me/#/brands' in self.driver.current_url
        self.driver.back()
        assert self.driver.current_url == URL
    
    def testReviewCard(self):
        self.driver.find_element(By.ID, 'reviews').click()
        assert 'https://dev.mytechreview.me/#/reviews' in self.driver.current_url
        self.driver.back()
        assert self.driver.current_url == URL
    
    def testNavBar(self):
        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/a")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[1]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[2]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/products'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[3]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/brands'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[4]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/reviews'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[5]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/about'

    def testSearching(self):
        self.driver.find_elements_by_class_name("btn")[0].click()
        assert "https://dev.mytechreview.me/#/search?search=" in self.driver.current_url

if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])
