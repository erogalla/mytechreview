import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import sys
# from webdriver_manager.chrome import ChromeDriverManager

# TO RUN:
# python ./front-end/src/selenium_tests/AboutTests.py

# Inspired by CulturedFoodies: https://gitlab.com/cs373-group-11/cultured-foodies/-/blob/master/frontend/src/tests/selenium_tests.py

URL = 'https://dev.mytechreview.me/#/about'
PATH = "./selenium_tests/chromedriver.exe"

class Selenium_Tests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        # chrome_options.add_argument('--disable-dev-shm-usage')
        # service = Service('./chromedriver.exe')
        # PATH = "./selenium_tests/chromedriver.exe"
        self.driver = webdriver.Chrome(executable_path=PATH, options=chrome_options) #webdriver.Chrome(options=chrome_options, service=service)
        self.driver.get(URL)
        self.driver.implicitly_wait(40)
    
    def tearDown(self):
        self.driver.quit()

    def testTitle(self):
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == 'About MyTechReview'
    
    def testNavBar(self):
        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/a")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[1]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[2]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/products'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[3]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/brands'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[4]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/reviews'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[5]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/about'

if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])
