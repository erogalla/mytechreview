import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
import sys

# TO RUN:
# python ./front-end/src/selenium_tests/ProductsTests.py

# Inspired by CulturedFoodies: https://gitlab.com/cs373-group-11/cultured-foodies/-/blob/master/frontend/src/tests/selenium_tests.py
PATH = "./selenium_tests/chromedriver.exe"
URL = 'https://dev.mytechreview.me/#/products' #https://www.mytechreview.me/#/products'

class Selenium_Tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        # chrome_options.add_argument('--disable-dev-shm-usage')
        # service = Service('./chromedriver.exe')
        # PATH = "./selenium_tests/chromedriver.exe"
        self.driver = webdriver.Chrome(executable_path=PATH, options=chrome_options) #webdriver.Chrome(options=chrome_options, service=service)
        self.driver.get(URL)
        self.driver.implicitly_wait(80)
    
    @classmethod
    def tearDown(self):
        self.driver.quit()

    def testTitle(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(By.TAG_NAME, 'h1')
            )
        except Exception as ex:
            print(ex)
            return
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        assert element.text == "All Tech Review Products"
    
    # TODO: change test to include clicking on whole row
    def testFirstRow(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(By.ID, '1')
            )
        except Exception as ex:
            print(ex)
            return
        self.driver.find_element(By.ID, '1').click()
        assert 'https://dev.mytechreview.me/#/product/1' in self.driver.current_url
        self.driver.back()
        assert self.driver.current_url == URL
    
    def testNavBar(self):
        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/a")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[1]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[2]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/products'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[3]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/brands'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[4]")[0].click()
        assert self.driver.current_url == 'https://dev.mytechreview.me/#/reviews'

        self.driver.find_elements_by_xpath("/html/body/div/div[1]/nav/div/div/a[5]")[0].click()
        self.driver.current_url == 'https://dev.mytechreview.me/#/about'

    def testFiltering(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(By.ID, 'brand_input')
            )
        except Exception as ex:
            print(ex)
            return
        self.driver.find_element(By.ID, 'brand_input').click()
        self.driver.find_elements_by_class_name('highlightOption')[0].click()
        assert 'https://dev.mytechreview.me/#/products?brand=11%20bit%20studios' in self.driver.current_url
    
    def testSorting(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(By.ID, 'Product Name')
            )
        except Exception as ex:
            print(ex)
            return
        self.driver.find_element(By.ID, 'Product Name').click()
        assert 'https://dev.mytechreview.me/#/products?sort=name' in self.driver.current_url
        try:
            self.driver.find_element(By.ID, '1')
            assert False
        except:
            assert True
        try:
            self.driver.find_element(By.ID, '663')
            assert True
        except:
            assert False
    
    def testSearching(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(By.CLASS_NAME, 'nav-item')
            )
        except Exception as ex:
            print(ex)
            return
        self.driver.find_elements_by_class_name("nav-item")[1].click()
        assert "https://dev.mytechreview.me/#/products-search" in self.driver.current_url
        element = self.driver.find_elements_by_class_name("form-outline")[0]
        assert element is not None



if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])
