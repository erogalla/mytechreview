import { AboutInfoType } from '../types/AboutInfoType';

import PostmanLogo from '../assets/Images/PostmanLogo.png';
import GitLabLogo from '../assets/Images/GitLabLogo.png';
import BestBuyLogo from '../assets/Images/BestBuyLogo.png';
import MediaWikiLogo from '../assets/Images/MediaWikiLogo.png';
import GoogleDevLogo from '../assets/Images/GoogleDevLogo.png';
import ReactLogo from '../assets/Images/ReactLogo.png';
import BootstrapLogo from '../assets/Images/BootstrapLogo.png';
import AWSLogo from '../assets/Images/AWSLogo.png';
import DockerLogo from '../assets/Images/DockerLogo.png';
import NameCheapLogo from '../assets/Images/NameCheapLogo.png';
import PrettierLogo from '../assets/Images/PrettierLogo.png';
import JestLogo from '../assets/Images/JestLogo.png';
import SeleniumLogo from '../assets/Images/SeleniumLogo.png';
import UnittestLogo from '../assets/Images/UnittestLogo.png';
import FlaskLogo from '../assets/Images/FlaskLogo.png';
import SQLALogo from '../assets/Images/SQLALogo.png';
import PostgreLogo from '../assets/Images/PostgreLogo.png';
import DiscordLogo from '../assets/Images/DiscordLogo.png';

const data: AboutInfoType[] = [
  {
    title: 'BestBuy',
    description: 'Contains data about various electronic Best Buy products, brands, and rating',
    img: BestBuyLogo,
    link: 'https://bestbuyapis.github.io/api-documentation/#detail',
  },
  {
    title: 'MediaWiki',
    description: 'Contains data about various electronic products and brands',
    img: MediaWikiLogo,
    link: 'https://www.mediawiki.org/wiki/API:Main_page',
  },
  {
    title: 'Google Knowledge Graph',
    description: 'Contains data about various electronic products and brands',
    img: GoogleDevLogo,
    link: 'https://developers.google.com/knowledge-graph',
  },
];

const tools: AboutInfoType[] = [
  {
    title: 'React',
    description: 'JavaScript library used for front-end development',
    img: ReactLogo,
    link: 'https://reactjs.org/',
  },
  {
    title: 'Bootstrap',
    description: 'CSS framework used for front-end development',
    img: BootstrapLogo,
    link: 'https://getbootstrap.com/',
  },
  {
    title: 'Postman',
    description: 'API platform used to create our API documentation',
    img: PostmanLogo,
    link: 'https://www.postman.com/',
  },
  {
    title: 'AWS',
    description: 'Cloud computing service used to host our website',
    img: AWSLogo,
    link: 'https://aws.amazon.com/',
  },
  {
    title: 'Docker',
    description: 'Developer tool used to create frontend docker image',
    img: DockerLogo,
    link: 'https://www.docker.com/',
  },
  {
    title: 'Namecheap',
    description: 'Domain name registrar used to create our URL',
    img: NameCheapLogo,
    link: 'https://www.namecheap.com/',
  },
  {
    title: 'GitLab',
    description: 'DevOps platform used to manage our repository',
    img: GitLabLogo,
    link: 'https://gitlab.com/',
  },
  {
    title: 'Discord',
    description: 'Communication platform used to communicate with our team members',
    img: DiscordLogo,
    link: 'https://discord.com/',
  },
  {
    title: 'Prettier',
    description: 'Code formatter',
    img: PrettierLogo,
    link: 'https://prettier.io/',
  },
  {
    title: 'Jest',
    description: 'Javascript testing framework used to create unit tests of the JavaScript code',
    img: JestLogo,
    link: 'https://jestjs.io/',
  },
  {
    title: 'Selenium',
    description: 'Suite of tools used to create acceptance tests of the GUI',
    img: SeleniumLogo,
    link: 'https://www.selenium.dev/',
  },
  {
    title: 'Unittest',
    description: 'Python unit testing framework used to create unit tests of the Python code',
    img: UnittestLogo,
    link: 'https://docs.python.org/3/library/unittest.html',
  },
  {
    title: 'Flask',
    description:
      'Micro web framework used as a Web framework for the backend server and to allow the frontend to consume the API',
    img: FlaskLogo,
    link: 'https://flask.palletsprojects.com/en/2.0.x/',
  },
  {
    title: 'SQLAlchemy',
    description: 'SQL toolkit used as a Web framework for the backend server',
    img: SQLALogo,
    link: 'https://www.sqlalchemy.org/',
  },
  {
    title: 'PostgreSQL',
    description: 'Relational database management system used for our database',
    img: PostgreLogo,
    link: 'https://www.postgresql.org/',
  },
];

const repoAndApi: AboutInfoType[] = [
  {
    title: 'GitLab',
    description: 'Our Repo',
    img: GitLabLogo,
    link: 'https://gitlab.com/erogalla/mytechreview/-/tree/main',
  },
  {
    title: 'Postman',
    description: 'Our API',
    img: PostmanLogo,
    link: 'https://documenter.getpostman.com/view/17710111/UUxzC8rz',
  },
];

export { repoAndApi, tools, data };
