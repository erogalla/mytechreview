import BrandType from '../types/BrandType';
import ProductType from '../types/ProductType';
import ReviewType from '../types/ReviewType';

import { PaginatedProductType, PaginatedBrandType, PaginatedReviewType } from '../types/PaginatedType';

let apiURL = 'https://api.mytechreview.me';
if (process.env?.REACT_APP_USING_LOCAL_API === 'TRUE') {
  console.log('running on local api');
  apiURL = 'http://localhost:8000';
}

const providerApiURL = 'https://api.bookipedia.me/api';

// Args cant contain paginate
export const getBrandList = async (args?: string): Promise<BrandType[]> => {
  if (args && args.includes('paginate')) throw Error('Call getPaginatedBrandList');
  const brandList: BrandType[] = await fetch(`${apiURL}/brands${args ? '?' + args : ''}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  // const productList = await getProductList();

  // const reviewMapping = productList.reduce((mapping, product) => {
  //   if (!mapping[product.brandId]) {
  //     mapping[product.brandId] = { rating: product.rating, numReviews: product.numReviews };
  //   } else {
  //     mapping[product.brandId] = {
  //       rating:
  //         (product.rating * mapping[product.brandId].numReviews + mapping[product.brandId].rating) /
  //         (product.numReviews + mapping[product.brandId].numReviews),
  //       numReviews: product.numReviews + mapping[product.brandId].numReviews,
  //     };
  //   }
  //   return mapping;
  // }, {} as { [brandId: number]: { rating: number; numReviews: number } });

  // brandList = brandList.map((brand) => {
  //   const review = reviewMapping[brand.id];
  //   if (review) {
  //     return {
  //       ...brand,
  //       rating: review.rating,
  //       numReviews: review.numReviews,
  //     } as BrandType;
  //   } else {
  //     return brand;
  //   }
  // });

  return brandList;
};

export const getPaginatedBrandList = async (args?: string): Promise<PaginatedBrandType> => {
  const paginatedBrandData: PaginatedBrandType = await fetch(`${apiURL}/brands${args ? '?' + args : ''}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  // const productList = await getProductList();

  // const reviewMapping = productList.reduce((mapping, product) => {
  //   if (!mapping[product.brandId]) {
  //     mapping[product.brandId] = { rating: product.rating, numReviews: product.numReviews };
  //   } else {
  //     mapping[product.brandId] = {
  //       rating:
  //         (product.rating * mapping[product.brandId].numReviews + mapping[product.brandId].rating) /
  //         (product.numReviews + mapping[product.brandId].numReviews),
  //       numReviews: product.numReviews + mapping[product.brandId].numReviews,
  //     };
  //   }
  //   return mapping;
  // }, {} as { [brandId: number]: { rating: number; numReviews: number } });

  // paginatedBrandData.data = paginatedBrandData.data.map((brand) => {
  //   const review = reviewMapping[brand.id];
  //   if (review) {
  //     return {
  //       ...brand,
  //       rating: review.rating,
  //       numReviews: review.numReviews,
  //     } as BrandType;
  //   } else {
  //     return brand;
  //   }
  // });

  return paginatedBrandData;
};

export const getBrand = async (id: string): Promise<BrandType> => {
  const brand = await fetch(apiURL + '/brand/id=' + id)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return {};
    });
  return brand;
};

type BrandSearchType = {
  data: BrandType[];
  totalCount: number;
};

export const getBrandBySearch = async (search: string, page: number): Promise<BrandSearchType> => {
  if (search === '') {
    return { data: [], totalCount: 0 };
  }
  const brandList = await fetch(apiURL + '/brands/search?search=' + search + '&page=' + page)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return { data: [], totalCount: 0 };
    });
  return brandList;
};

export const getProductList = async (args?: string): Promise<ProductType[]> => {
  if (args && args.includes('paginate')) throw Error('Call getPaginatedProductList');
  const productList: ProductType[] = await fetch(`${apiURL}/products${args ? '?' + args : ''}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  // const reviewList: ReviewType[] = await getReviewList();
  // productList = productList.map((product) => {
  //   const reviews = reviewList.filter((review) => review.productId === product.id);
  //   return {
  //     ...product,
  //     numReviews: reviews.length,
  //     rating: reviews.length !== 0 ? reviews.reduce((a, b) => a + b.numStars, 0) / reviews.length : 0,
  //   };
  // });

  return productList;
};

export const getPaginatedProductList = async (args?: string): Promise<PaginatedProductType> => {
  const paginatedProductData: PaginatedProductType = await fetch(`${apiURL}/products${args ? '?' + args : ''}`)
    .then((response) => {
      const js = response.json();
      console.log('response', js);
      return js;
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  // const reviewList: ReviewType[] = await getReviewList();
  // paginatedProductData.data = paginatedProductData.data.map((product) => {
  //   const reviews = reviewList.filter((review) => review.productId === product.id);
  //   return {
  //     ...product,
  //     numReviews: reviews.length,
  //     rating: reviews.length !== 0 ? reviews.reduce((a, b) => a + b.numStars, 0) / reviews.length : 0,
  //   };
  // });

  return paginatedProductData;
};

export const getProduct = async (id: string): Promise<ProductType> => {
  const product = await fetch(apiURL + '/product/id=' + id)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return {};
    });
  return product;
};

export const getProductsByBrand = async (id: string): Promise<ProductType[]> => {
  const products = await fetch(apiURL + '/product/brand_id=' + id)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  return products;
};

type ProductSearchType = {
  data: ProductType[];
  totalCount: number;
};

export const getProductsBySearch = async (search: string, page: number): Promise<ProductSearchType> => {
  if (search === '') {
    return { data: [], totalCount: 0 };
  }
  const products = await fetch(apiURL + '/products/search?search=' + search + '&page=' + page)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return { data: [], totalCount: 0 };
    });
  return products;
};

export const getReviewList = async (args?: string): Promise<ReviewType[]> => {
  if (args && args.includes('paginate')) throw Error('Call getPaginatedReviewList');
  const reviewList = await fetch(`${apiURL}/reviews${args ? '?' + args : ''}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  return reviewList;
};

export const getPaginatedReviewList = async (args?: string): Promise<PaginatedReviewType> => {
  const paginatedReviewData: PaginatedReviewType = await fetch(`${apiURL}/reviews${args ? '?' + args : ''}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  return paginatedReviewData;
};

export const getReview = async (id: string): Promise<ReviewType> => {
  const review = await fetch(apiURL + '/review/id=' + id)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return {};
    });
  return review;
};

export const getReviewsByProductId = async (productID: number): Promise<ReviewType[]> => {
  const reviews = await fetch(apiURL + '/review/product_id=' + productID)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  return reviews;
};

// TODO: Fix because this should not be handled on the frontend
export const getReviewsByBrandId = async (brandID: number): Promise<ReviewType[]> => {
  const reviews = await fetch(apiURL + '/review/brand_id=' + brandID)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  return reviews;
};

type ReviewSearchType = {
  data: ReviewType[];
  totalCount: number;
};

export const getReviewsBySearch = async (search: string, page: number): Promise<ReviewSearchType> => {
  if (search === '') {
    return { data: [], totalCount: 0 };
  }
  const products = await fetch(apiURL + '/reviews/search?search=' + search + '&page=' + page)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return { data: [], totalCount: 0 };
    });
  return products;
};

export type SearchType = {
  products: ProductType[];
  numProducts: number;
  brands: BrandType[];
  numBrands: number;
  reviews: ReviewType[];
  numReviews: number;
};

export const emptySearch = {
  products: [],
  numProducts: 0,
  brands: [],
  numBrands: 0,
  reviews: [],
  numReviews: 0,
};

export const getSearch = async (search: string): Promise<SearchType> => {
  if (search === '') {
    return emptySearch;
  }
  const products = await fetch(apiURL + '/search?search=' + search)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return emptySearch;
    });
  return products;
};

type BookipediaAuthorApitResponse = {
  authors: Author[];
};

export type Author = {
  first_name: string;
  last_name: string;
  num_works: number;
};

export const getAuthors = async (): Promise<BookipediaAuthorApitResponse> => {
  const authors = await fetch(providerApiURL + '/all_authors')
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  return authors;
};

type BookApiResponse = {
  books: Book[];
};

export type Book = {
  published_date: string;
};
published_date: 'string';

export const getBooks = async (): Promise<BookApiResponse> => {
  const books = await fetch(providerApiURL + '/all_books')
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  return books;
};

type BookipediaLibraryApiResponse = {
  libraries: Library[];
};

export type Library = {
  libname: string;
  ser_pop: number;
};

export const getLibraries = async (): Promise<BookipediaLibraryApiResponse> => {
  const libraries = await fetch(providerApiURL + '/all_libraries')
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
      return [];
    });

  return libraries;
};
