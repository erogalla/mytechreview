// Types
import {
  CommitsApiCallType,
  GitlabStatsType,
  IssuesApiCallType,
  GitlabTestsType,
  PipelineApiCallType,
  TestReportApiCallType,
} from '../types/GitlabTypes';

const baseUrl = 'https://gitlab.com/api/v4/projects/29882333';
const contributorsEndPoint = `${baseUrl}/repository/contributors`;
const issuesEndPoint = `${baseUrl}/issues?per_page=100`;
const pipelineEndPoint = `${baseUrl}/pipelines?ref=main`;
// const contributorsEndPoint = 'https://gitlab.com/api/v4/projects/29882333/repository/contributors';
// const issuesEndPoint = 'https://gitlab.com/api/v4/projects/29882333/issues';

export const fetchGitLabStats = async (gitLabStats: GitlabStatsType[]): Promise<GitlabStatsType[]> => {
  const commits: CommitsApiCallType[] = await fetch(contributorsEndPoint).then((response) => response.json());
  const issues: IssuesApiCallType[] = await fetch(issuesEndPoint).then((response) => response.json());

  // export const fetchGitLabStats = async (): Promise<GitlabStatsType[]> => {
  //   const gitLabStats = teamMemberInfo; // Start with original data

  //   const commits: CommitsApiCallType[] = await fetch(contributorsEndPoint).then((response) => response.json());
  //   const issues: IssuesApiCallType[] = await fetch(issuesEndPoint).then((response) => response.json());

  commits.forEach((commit) => {
    const email = commit.email;
    const num_commits = commit.commits;
    gitLabStats.forEach((teamMember) => {
      if (teamMember.email === email) {
        teamMember.commits = num_commits;
      }
    });
  });

  issues.forEach((issue) => {
    if (issue.assignee !== null) {
      const username = issue.assignee.username;
      gitLabStats.forEach((teamMember) => {
        if (teamMember.username === username) {
          teamMember.issues += 1;
        }
      });
    }
  });

  return gitLabStats;
};

export const fetchGitLabTests = async (): Promise<GitlabTestsType> => {
  // TODO: Figure out if we care about displaying num commits other than main
  const pipelines: PipelineApiCallType[] = await fetch(pipelineEndPoint).then((response) => response.json());
  const testReport = await fetch(`${baseUrl}/pipelines/${pipelines[0].id}/test_report`)
    .then((response) => response.json())
    .then((data) => data as TestReportApiCallType);

  return { numTests: testReport.total_count };
};
