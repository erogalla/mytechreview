// Types
import { GitlabStatsType } from '../types/GitlabTypes';

// Images
import SarahProfileImg from '../assets/ProfileImages/SarahProfileImgCircle.png';
import SafinProfileImg from '../assets/ProfileImages/SafinProfileImgCircle.png';
import EmilioProfileImg from '../assets/ProfileImages/EmilioProfileImgCircle.png';
import ShizukaProfileImg from '../assets/ProfileImages/ShizukaProfileImgCircle.png';
import DanielProfileImg from '../assets/ProfileImages/DanielProfileImgCircle.png';

const teamMemberInfo: GitlabStatsType[] = [
  {
    name: 'Sarah Zhang',
    email: 'sarahz00@yahoo.com',
    username: 'sarahz00',
    img: SarahProfileImg,
    role: 'Front-end',
    bio: "I'm a third year Computer Science major at the University of Texas at Austin. Outside of school, I enjoy baking and running.",
    commits: 0,
    issues: 0,
    tests: 22,
  },
  {
    name: 'Md Safin al Wasi',
    email: 'safinwasi_11@outlook.com',
    username: 'SafinWasi',
    img: SafinProfileImg,
    role: 'Back-end',
    bio: "I'm a senior CS undergrad here at UT. I'm an international student from Bangladesh, and in my free time I play video games and listen to music.",
    commits: 0,
    issues: 0,
    tests: 18,
  },
  {
    name: 'Emilio Rogalla',
    email: 'erogalla@utexas.edu',
    username: 'erogalla',
    img: EmilioProfileImg,
    role: 'Front-end',
    bio: "I'm a 3rd year CS student at UT. I enjoy learning languages and hope to participate in a study abroad in Korea during college.",
    commits: 0,
    issues: 0,
    tests: 24,
  },
  {
    name: 'Shizuka Bai',
    email: 'shizukabai@utexas.edu',
    username: 'Shizuka0000',
    img: ShizukaProfileImg,
    role: 'Back-end',
    bio: "I'm a senior in Computer Science major at University of Texas Austin. I enjoy literary translation and playing co-op games with my friends outside of class.",
    commits: 0,
    issues: 0,
    tests: 49,
  },
  {
    name: 'Daniel Cai',
    email: 'danielhcai@gmail.com',
    username: 'danielhcai',
    img: DanielProfileImg,
    role: 'Front-end',
    bio: "I'm a third year computer science major at UT. Other than classes, I enjoy playing badminton and working out.",
    commits: 0,
    issues: 0,
    tests: 0,
  },
];

export default teamMemberInfo;
