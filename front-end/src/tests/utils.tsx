// https://testing-library.com/docs/example-react-router
/* eslint-disable*/ 

import React from 'react';
import { Route, Router } from 'react-router-dom';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';



// test utils file
const renderWithRouter = (ui: JSX.Element, path: string, route: string) => { 
  const history = createMemoryHistory({ initialEntries: [route] });
  return { 
    ...render( 
      <Router history={history}> 
        <Route path={path}>
          {ui}
        </Route>
      </Router> 
    ) 
  };
}

export default renderWithRouter;