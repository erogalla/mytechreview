import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import App from '../App';

describe('Navbar Testing', () => {
  it('Has a Navbar', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toBeInTheDocument();
  });

  it('Displays Home Link', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toHaveTextContent('My Tech Review');
  });

  it('Displays Products Link', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toHaveTextContent('Products');
  });

  it('Displays Brands Link', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toHaveTextContent('Brands');
  });

  it('Displays Reviews Link', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toHaveTextContent('Reviews');
  });

  it('Displays About Link', () => {
    render(<App />);
    expect(screen.getByRole('navigation')).toHaveTextContent('About');
  });
});

describe('Splash Page Testing', () => {
  it('Has 3 Splash models', () => {
    const { container } = render(<App />);
    const models = container.getElementsByClassName('model');
    expect(models.length).toBe(3);
  });

  it('Has 3 Splash model buttons', () => {
    const { container } = render(<App />);
    const modelButtons = container.getElementsByClassName('model-button');
    expect(modelButtons.length).toBe(3);
  });

  it('Can Click Button', () => {
    const { container } = render(<App />);

    const button = container.getElementsByClassName('model-button');

    fireEvent.click(button[0]);
  });
});
