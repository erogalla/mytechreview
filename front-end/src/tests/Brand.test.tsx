import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import renderWithRouter from './utils';

import Brand from '../components/Models/Brands/Brand';

describe('Single Brand and Brands Table', () => {
  it('Displays Apple Brand Card', async () => {
    renderWithRouter(<Brand />, '/brand/:brandID', '/brand/0');
    await waitFor(() => screen.getByText(/Founded in: /i));
  });

  it('Displays No Brand Card', async () => {
    renderWithRouter(<Brand />, '/brand/:brandID', '/brand/-1');
    await waitFor(() => screen.getByText('No Brand available.'));
  });
});
