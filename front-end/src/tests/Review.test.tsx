import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import renderWithRouter from './utils';

import Review from '../components/Models/Reviews/Review';
import ReviewsTable from '../components/Models/Reviews/ReviewsTable';
import ReviewSearch from '../components/Models/Reviews/ReviewsSearch';

describe('Single Review and Reviews Table', () => {
  it('Displays First Review Card', async () => {
    renderWithRouter(<Review />, '/review/:reviewId', '/review/0');
    await waitFor(() => screen.getByText(/found this helpful /i));
  });

  it('Displays No Product Card', async () => {
    renderWithRouter(<Review />, '/review/:reviewId', '/review/-1');
    await waitFor(() => screen.getByText('No Review available.'));
  });

  it('Renders Reviews Table', () => {
    const { container } = renderWithRouter(<ReviewsTable />, '', '');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Reviews Table with sort', () => {
    const { container } = renderWithRouter(<ReviewsTable />, '', '?sort=rating');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Reviews Table with filter', () => {
    const { container } = renderWithRouter(<ReviewsTable />, '', '?rating=5');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Review Search', async () => {
    renderWithRouter(<ReviewSearch />, '', '');
    await waitFor(() => screen.getByText('Search Reviews'));
  });
});
