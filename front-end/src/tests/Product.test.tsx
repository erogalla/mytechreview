import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import renderWithRouter from './utils';

import Product from '../components/Models/Products/Product';
import ProductsTable from '../components/Models/Products/ProductsTable';
import ProductSearch from '../components/Models/Products/ProductsSearch';

describe('Single Product and Products Table', () => {
  it('Displays first product', async () => {
    renderWithRouter(<Product />, '/product/:productId', '/product/0');
    await waitFor(() => screen.getByText(/Price: /i));
  });

  it('Displays No Product Card', async () => {
    renderWithRouter(<Product />, '/product/:productId', '/product/-1');
    await waitFor(() => screen.getByText('No Product available.'));
  });

  it('Renders Product Table', () => {
    const { container } = renderWithRouter(<ProductsTable />, '', '');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Product Table with sort', () => {
    const { container } = renderWithRouter(<ProductsTable />, '', '?sort=os');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Product Table with filter', () => {
    const { container } = renderWithRouter(<ProductsTable />, '', '?ram=4');

    // Ensure only 1 table element
    const table = container.getElementsByClassName('table');
    expect(table.length).toBe(1);
  });

  it('Renders Product Search', async () => {
    renderWithRouter(<ProductSearch />, '', '');
    await waitFor(() => screen.getByText('Search Products'));
  });
});
