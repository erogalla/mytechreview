import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../App';

describe('Navbar Testing', () => {
  test('renders learn react link', () => {
    render(<App />);
    const linkElement = screen.getByText('Home');
    expect(linkElement).toBeInTheDocument();
  });
});
