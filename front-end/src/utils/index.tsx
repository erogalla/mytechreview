import queryString from 'query-string';
import _ from 'underscore';

export const queryStringsByPrefix = (search: string, prefix: string): string[] => {
  const allQueryStrings = queryString.parse(search);
  const selectedQueryStrings = _.pick(allQueryStrings, (value, key) => key.includes(prefix));
  const queryStrings = Object.values(selectedQueryStrings)
    .flatMap((selectedQueryString) => selectedQueryString)
    .filter((queryString) => queryString) as string[];

  return queryStrings;
};

// Return search that exist filtered options
export const cleanSearch = (search: string, filter: { [type: string]: string[] }): string => {
  const allQueryStrings = queryString.parse(search);
  const filteredQueryStrings = _.mapObject(allQueryStrings, (values, key) => {
    if (typeof values === 'string') {
      values = [values];
    }
    return values?.filter((value) => filter[key].length === 0 || filter[key].includes(value));
  });

  return queryString.stringify(filteredQueryStrings);
};

export const searchContains = (search: string, attribute: string, value?: string): boolean => {
  const allQueryStrings = queryString.parse(search);
  if (value) {
    Object.keys(allQueryStrings).includes(attribute) && allQueryStrings[attribute] === value;
  }
  return Object.keys(allQueryStrings).includes(attribute);
};

export const searchWithQueryString = (search: string, key: string, value: string): string => {
  const newSearch = queryString.stringify(queryString.parse(`${search}&${key}=${value}`));
  return newSearch;
};

export const searchWithoutQueryString = (search: string, key: string, value?: string): string => {
  let rawSearch;
  if (value) {
    const newField = `${key}=${value.replaceAll(' ', '%20')}`;
    const idx = search.indexOf(newField);
    rawSearch = search.substring(0, idx) + search.substring(idx + newField.length);
  } else {
    const start = search.indexOf(key);
    if (start === -1) {
      rawSearch = search;
    } else {
      let end = search.indexOf('&', start);
      if (end < 0) end = search.length;
      rawSearch = search.substring(0, start) + search.substring(end);
    }
  }

  const newSearch = queryString.stringify(queryString.parse(rawSearch));
  return newSearch;
};
