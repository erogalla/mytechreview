import Brand from '../components/Models/Brands/Brand';
import BrandsTable from '../components/Models/Brands/BrandsTable';
import BrandsSearch from '../components/Models/Brands/BrandsSearch';
import PageContainer from '../components/Components/PageContainer';
import Product from '../components/Models/Products/Product';
import ProductsTable from '../components/Models/Products/ProductsTable';
import ProductsSearch from '../components/Models/Products/ProductsSearch';
import Review from '../components/Models/Reviews/Review';
import ReviewsTable from '../components/Models/Reviews/ReviewsTable';
import ReviewsSearch from '../components/Models/Reviews/ReviewsSearch';
import Splash from '../components/Splash/Splash';
import About from '../components/About/About';
import GeneralSearch from '../components/Search/GeneralSearch';
import Visualizations from '../components/Visualizations/OurVisualizations';
import ProviderVisualizations from '../components/Visualizations/ProviderVisualizations';

export const BrandRoute = (): JSX.Element => PageContainer({ children: Brand() });
export const BrandsTableRoute = (): JSX.Element => PageContainer({ children: BrandsTable() });
export const BrandsSearchRoute = (): JSX.Element => PageContainer({ children: BrandsSearch() });

export const ProductRoute = (): JSX.Element => PageContainer({ children: Product() });
export const ProductsTableRoute = (): JSX.Element => PageContainer({ children: ProductsTable() });
export const ProductsSearchRoute = (): JSX.Element => PageContainer({ children: ProductsSearch() });

export const ReviewRoute = (): JSX.Element => PageContainer({ children: Review() });
export const ReviewsTableRoute = (): JSX.Element => PageContainer({ children: ReviewsTable() });
export const ReviewsSearchRoute = (): JSX.Element => PageContainer({ children: ReviewsSearch() });

export const SplashRoute = (): JSX.Element => PageContainer({ children: Splash() });

export const AboutRoute = (): JSX.Element => PageContainer({ children: About() });

export const SearchRoute = (): JSX.Element => PageContainer({ children: GeneralSearch() });

export const VisualizationsRoute = (): JSX.Element => PageContainer({ children: Visualizations() });

export const ProviderVisualizationsRoute = (): JSX.Element => PageContainer({ children: ProviderVisualizations() });
