import React from 'react';
import { useState, useEffect } from 'react';
import Styled from 'styled-components';
import { getProductList } from '../../api';
import LoadingImage from '../Components/LoadingImage';
import { ProductFilterOptions } from '../Filter/FilterOptions';
import { Radar, RadarChart, PolarGrid, Legend, PolarAngleAxis, PolarRadiusAxis } from 'recharts';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const productFilters = ProductFilterOptions[3];

type productsPerPriceType = { [key: string]: number };

const ProductVizualization = (): JSX.Element => {
  const [productsPerPrice, setProductsPerPrice] = useState<productsPerPriceType>({});

  useEffect(() => {
    const getData = () => {
      setProductsPerPrice({});
      productFilters.map(async (priceRange) => {
        const productsList = await getProductList('priceRange=' + priceRange);
        setProductsPerPrice((productsPerPrice) => ({ ...productsPerPrice, [priceRange]: productsList.length }));
      });
    };
    getData();
  }, []);

  let visualization = null;
  if (Object.keys(productsPerPrice).length > 0) {
    visualization = <ProductChart Products={productsPerPrice} />;
  } else {
    visualization = <LoadingImage />;
  }

  return (
    <div>
      <Header>
        <h3 className="font-blue">Number of Products By Price Range</h3>
      </Header>
      <br />
      {visualization}
    </div>
  );
};

const ProductChart = ({ Products }: { Products: productsPerPriceType }): JSX.Element => {
  const data = productFilters.map((p) => {
    return { priceRange: p, value: Products[p] };
  });

  return (
    <RadarChart cx={250} cy={200} outerRadius={150} width={500} height={450} data={data} style={{ margin: 'auto' }}>
      <PolarGrid gridType="circle" />
      <PolarAngleAxis dataKey="priceRange" />
      <PolarRadiusAxis angle={30} domain={[0, 50]} />
      <Radar name="Products per Price Range" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
      <Legend />
    </RadarChart>
  );
};

export default ProductVizualization;
