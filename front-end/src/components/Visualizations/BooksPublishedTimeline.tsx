import React, { useEffect, useState } from 'react';

import Chart from 'react-chartjs-2';
import Spinner from 'react-bootstrap/Spinner';
import Dropdown from 'react-bootstrap/Dropdown';

import { getBooks, Book } from '../../api';

type timeframeType = 'All Time' | 'Yearly' | 'Monthly';
type monthsType =
  | 'January'
  | 'February'
  | 'March'
  | 'April'
  | 'May'
  | 'June'
  | 'July'
  | 'August'
  | 'September'
  | 'October'
  | 'November'
  | 'December';

const months: monthsType[] = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const BooksPublishedTimeline = (): JSX.Element => {
  const [label, setLabel] = useState('');
  const [labels, setLabels] = useState<string[]>([]);
  const [timeFrame, setTimeFrame] = useState<timeframeType>('All Time');
  const [selectedMonth, setSelectedMonth] = useState<monthsType>('January');
  const [selectedYear, setSelectedYear] = useState<number>(2021);
  const [selectedBooks, setSelectedBooks] = useState<number[]>([]);

  const [books, setBooks] = useState<Book[]>([]);
  const [earliestBookYear, setEarliestBookYear] = useState(2021);
  const [latestBookYear, setLatestBookYear] = useState(2021);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const callBooksApi = async () => {
      const books = (await getBooks()).books;
      applyFilters(books, timeFrame, selectedMonth, selectedYear);
      setEarliestBookYear(Math.min(...books.map((book) => new Date(book.published_date).getFullYear())));
      setLatestBookYear(Math.max(...books.map((book) => new Date(book.published_date).getFullYear())));
    };
    callBooksApi();
  }, []);

  const applyFilters = (
    newBooks: Book[],
    newTimeFrame: timeframeType,
    newSelectedMonth: monthsType,
    newSelectedYear: number,
  ) => {
    // Create the labels based on time frame
    let newLabel: string;
    let newLabels: string[];
    let newSelectedBooks: number[];

    if (newTimeFrame === 'All Time') {
      const earliestBookYear = Math.min(...newBooks.map((book) => new Date(book.published_date).getFullYear()));
      const latestBookYear = Math.max(...newBooks.map((book) => new Date(book.published_date).getFullYear()));
      newLabels = [
        ...Array(latestBookYear - earliestBookYear + 1)
          .fill(0)
          .map((val, index) => `${earliestBookYear + index}`),
      ];

      const booksByYear = newBooks.map((book) => new Date(book.published_date).getFullYear());
      newSelectedBooks = booksByYear.reduce((allTimeBooks, newBookYear) => {
        allTimeBooks[newBookYear - earliestBookYear] += 1;
        return allTimeBooks;
      }, Array(latestBookYear - earliestBookYear + 1).fill(0));
      newLabel = `All books by Year (${booksByYear.length})`;
    } else if (newTimeFrame === 'Yearly') {
      newLabels = months;

      const booksInYear = newBooks.filter((book) => {
        const submissionDate = new Date(book.published_date);
        return submissionDate.getFullYear() === newSelectedYear;
      });

      const booksByMonth = booksInYear.map((book) => new Date(book.published_date).getMonth());

      newSelectedBooks = booksByMonth.reduce((booksByMonth, newBookMonth) => {
        booksByMonth[newBookMonth] += 1;
        return booksByMonth;
      }, Array(12).fill(0));
      newLabel = `All books in ${newSelectedYear} (${booksByMonth.length})`;
    } else {
      const daysInMonth = new Date(newSelectedYear, months.indexOf(newSelectedMonth), 0).getDate();
      newLabels = [
        ...Array(daysInMonth)
          .fill(0)
          .map((val, index) => `${index}`),
      ];

      const booksInMonth = newBooks.filter((book) => {
        const submissionDate = new Date(book.published_date);
        return (
          submissionDate.getFullYear() === newSelectedYear &&
          submissionDate.getMonth() === months.indexOf(newSelectedMonth)
        );
      });

      const booksByDay = booksInMonth.map((book) => new Date(book.published_date).getDate());

      newSelectedBooks = booksByDay.reduce((booksByDay, newBookDay) => {
        booksByDay[newBookDay] += 1;
        return booksByDay;
      }, Array(daysInMonth).fill(0));

      newLabel = `All bookss in ${newSelectedMonth}, ${newSelectedYear} (${booksByDay.length})`;
    }

    setBooks(newBooks);
    setLabel(newLabel);
    setLabels(newLabels);
    setTimeFrame(newTimeFrame);
    setSelectedMonth(newSelectedMonth);
    setSelectedYear(newSelectedYear);
    setSelectedBooks(newSelectedBooks);
    setLoading(false);
  };

  const data = {
    labels,
    datasets: [
      {
        label,
        backgroundColor: 'rgba(128,0,0, 1)',
        borderColor: 'rgba(128,0,0, 0.6)',
        borderWidth: 3,
        data: selectedBooks,
        tension: 0.4,
      },
    ],
  };

  return (
    <>
      <h3 className="font-blue">Books published over time</h3>
      {loading ? (
        <Spinner animation="border" style={{ margin: '1rem auto' }} />
      ) : (
        <div>
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <Dropdown>
              <Dropdown.Toggle>Timeframe</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                <Dropdown.Item onClick={() => applyFilters(books, 'All Time', selectedMonth, selectedYear)}>
                  All Time
                </Dropdown.Item>
                <Dropdown.Item onClick={() => applyFilters(books, 'Yearly', selectedMonth, selectedYear)}>
                  Yearly
                </Dropdown.Item>
                <Dropdown.Item onClick={() => applyFilters(books, 'Monthly', selectedMonth, selectedYear)}>
                  Monthly
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle>Year</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                {[
                  ...Array(latestBookYear - earliestBookYear + 1)
                    .fill(0)
                    .map((val, index) => `${earliestBookYear + index}`),
                ].map((year) => (
                  <Dropdown.Item
                    key={year}
                    onClick={() => applyFilters(books, timeFrame, selectedMonth, parseInt(year))}
                  >
                    {year}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle>Month</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                {months.map((month) => (
                  <Dropdown.Item key={month} onClick={() => applyFilters(books, 'Monthly', month, selectedYear)}>
                    {month}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <Chart
            type="line"
            data={data}
            options={{
              responsive: true,
              onClick: (e, element) => {
                if (element && element[0]) {
                  const index: number = element[0].index;
                  if (timeFrame === 'All Time') {
                    applyFilters(books, 'Yearly', selectedMonth, earliestBookYear + index);
                  } else if (timeFrame === 'Yearly') {
                    applyFilters(books, 'Monthly', months[index], selectedYear);
                  }
                }
              },
            }}
          />
        </div>
      )}
    </>
  );
};

export default BooksPublishedTimeline;
