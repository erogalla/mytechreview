import React from 'react';
import { useState, useEffect } from 'react';
import Styled from 'styled-components';

import { getAuthors, Author } from '../../api';

import { Bar } from 'react-chartjs-2';
import Button from 'react-bootstrap/Button';
import LoadingImage from '../Components/LoadingImage';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const AuthorVizualization = (): JSX.Element => {
  const [allAuthors, setAllAuthors] = useState<Author[]>([]);
  const [authors, setAuthors] = useState<Author[]>([]);
  const [page, setPage] = useState<number>(1);
  const itemsPerPage = 25;

  const nextPage = () => {
    setPage(page + 1);
  };

  const lastPage = () => {
    setPage(page - 1);
  };

  useEffect(() => {
    const getAuthorsByNumWorks = async () => {
      const authors = await getAuthors();
      const authorsList = authors.authors;
      authorsList.sort((x, y) => y.num_works - x.num_works);
      console.log(authorsList);
      setAllAuthors(authorsList);
      setAuthors(authorsList.slice(0, itemsPerPage));
    };
    getAuthorsByNumWorks();
  }, []);

  useEffect(() => {
    setAuthors(allAuthors.slice((page - 1) * 25, page * 25));
  }, [page]);

  let visualization = null;
  if (authors.length > 0) {
    visualization = <AuthorBarGraph authors={authors} />;
  } else {
    visualization = <LoadingImage />;
  }

  return (
    <div>
      <Header>
        <h3 className="font-blue"> Authors By Number of Works</h3>
        <div>
          <Button variant="light" onClick={lastPage} disabled={page === 1}>
            {'<'}
          </Button>
          <Button variant="light" onClick={nextPage} disabled={page === Math.ceil(allAuthors.length / itemsPerPage)}>
            {'>'}
          </Button>
        </div>
      </Header>
      <br />
      {visualization}
    </div>
  );
};

const AuthorBarGraph = ({ authors }: { authors: Author[] }): JSX.Element => {
  const data = {
    labels: authors.map((a) => a.first_name + ' ' + a.last_name),
    datasets: [
      {
        label: 'Number of Works',
        backgroundColor: 'rgb(54, 162, 235, .7)',
        borderColor: 'rgb(54, 162, 235)',
        borderWidth: 1,
        data: authors.map((a) => a.num_works),
      },
    ],
    responsive: true,
    maintainAspectRatio: false,
    beginAtZero: true,
  };

  return (
    <div>
      <Bar
        data={data}
        options={{
          indexAxis: 'x',
          responsive: true,
          scales: {
            y: {
              min: 0,
              max: 10,
            },
          },
        }}
      />
    </div>
  );
};

export default AuthorVizualization;
