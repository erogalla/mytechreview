import React from 'react';
import { useState, useEffect } from 'react';
import Styled from 'styled-components';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import { getLibraries, Library } from '../../api';
import LoadingImage from '../Components/LoadingImage';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const LibraryVizualization = (): JSX.Element => {
  const [allLibraries, setAllLibraries] = useState<Library[]>([]);

  useEffect(() => {
    const getData = async () => {
      const libraries = await getLibraries();
      const librariesList = libraries.libraries;
      setAllLibraries(librariesList);
    };
    getData();
  }, []);

  let visualization = null;
  if (allLibraries.length > 0) {
    visualization = <LibraryBubble Libraries={allLibraries} />;
  } else {
    visualization = <LoadingImage />;
  }

  return (
    <div>
      <Header>
        <h3 className="font-blue"> Libraries By Service Population</h3>
      </Header>
      <br />
      {visualization}
    </div>
  );
};

const LibraryBubble = ({ Libraries }: { Libraries: Library[] }): JSX.Element => {
  const data = Libraries.map((l) => {
    return { label: l.libname, value: l.ser_pop };
  });
  console.log(data);

  return (
    <div>
      <BubbleChart
        graph={{
          zoom: 0.6,
        }}
        width={1100}
        height={800}
        showLegend={false}
        data={data}
        valueFont={{
          family: 'Raleway',
          size: 8,
          color: '#fff',
        }}
        labelFont={{
          family: 'Raleway',
          size: 10,
          color: '#fff',
        }}
      />
    </div>
  );
};

export default LibraryVizualization;
