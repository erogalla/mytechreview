import React from 'react';
import LibraryVizualization from './LibrariesChart';
import AuthorVizualization from './AuthorBarGraph';
import BooksPublishedTimeline from './BooksPublishedTimeline';
import '../Models/ModelStyle.css';

const ProviderVisualizations = (): JSX.Element => {
  return (
    <>
      <h1 className="model-title">Provider Visualizations</h1>
      <p className="model-intro">Checkout our visualizations of our provider&apos;s (Bookipedia) site.</p>
      <div style={{ width: '50%', display: 'flex', flexDirection: 'column', margin: 'auto', gap: '5rem' }}>
        <AuthorVizualization />
        <LibraryVizualization />
        <BooksPublishedTimeline />
      </div>
    </>
  );
};

export default ProviderVisualizations;
