import React, { useEffect, useState } from 'react';

import { Bar } from 'react-chartjs-2';
import Spinner from 'react-bootstrap/Spinner';
import Dropdown from 'react-bootstrap/Dropdown';

import { getReviewList } from '../../api';
import ReviewType from '../../types/ReviewType';

type timeframeType = 'All Time' | 'Yearly' | 'Monthly';
type monthsType =
  | 'January'
  | 'February'
  | 'March'
  | 'April'
  | 'May'
  | 'June'
  | 'July'
  | 'August'
  | 'September'
  | 'October'
  | 'November'
  | 'December';

const months: monthsType[] = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const ReviewsTimeline = (): JSX.Element => {
  const [label, setLabel] = useState('');
  const [labels, setLabels] = useState<string[]>([]);
  const [timeFrame, setTimeFrame] = useState<timeframeType>('All Time');
  const [selectedMonth, setSelectedMonth] = useState<monthsType>('January');
  const [selectedYear, setSelectedYear] = useState<number>(2021);
  const [selectedReviews, setSelectedReviews] = useState<number[]>([]);

  const [reviews, setReviews] = useState<ReviewType[]>([]);
  const [earliestReviewYear, setEarliestReviewYear] = useState(2021);
  const [latestReviewYear, setLatestReviewYear] = useState(2021);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getReviews = async () => {
      const reviews = await getReviewList();
      applyFilters(reviews, timeFrame, selectedMonth, selectedYear);
      setEarliestReviewYear(Math.min(...reviews.map((review) => new Date(review.submissionTime).getFullYear())));
      setLatestReviewYear(Math.max(...reviews.map((review) => new Date(review.submissionTime).getFullYear())));
    };
    getReviews();
  }, []);

  const applyFilters = (
    newReviews: ReviewType[],
    newTimeFrame: timeframeType,
    newSelectedMonth: monthsType,
    newSelectedYear: number,
  ) => {
    // Create the labels based on time frame
    let newLabel: string;
    let newLabels: string[];
    let newSelectedReviews: number[];

    if (newTimeFrame === 'All Time') {
      const earliestReviewYear = Math.min(...newReviews.map((review) => new Date(review.submissionTime).getFullYear()));
      const latestReviewYear = Math.max(...newReviews.map((review) => new Date(review.submissionTime).getFullYear()));
      newLabels = [
        ...Array(latestReviewYear - earliestReviewYear + 1)
          .fill(0)
          .map((val, index) => `${earliestReviewYear + index}`),
      ];

      const reviewsByYear = newReviews.map((review) => new Date(review.submissionTime).getFullYear());
      newSelectedReviews = reviewsByYear.reduce((allTimeReviews, newReviewYear) => {
        allTimeReviews[newReviewYear - earliestReviewYear] += 1;
        return allTimeReviews;
      }, Array(latestReviewYear - earliestReviewYear + 1).fill(0));
      newLabel = `All reviews by Year (${reviewsByYear.length})`;
    } else if (newTimeFrame === 'Yearly') {
      newLabels = months;

      const reviewsInYear = newReviews.filter((review) => {
        const submissionDate = new Date(review.submissionTime);
        return submissionDate.getFullYear() === newSelectedYear;
      });

      const reviewsByMonth = reviewsInYear.map((review) => new Date(review.submissionTime).getMonth());

      newSelectedReviews = reviewsByMonth.reduce((reviewsByMonth, newReviewMonth) => {
        reviewsByMonth[newReviewMonth] += 1;
        return reviewsByMonth;
      }, Array(12).fill(0));
      newLabel = `All reviews in ${newSelectedYear} (${reviewsByMonth.length})`;
    } else {
      const daysInMonth = new Date(newSelectedYear, months.indexOf(newSelectedMonth), 0).getDate();
      newLabels = [
        ...Array(daysInMonth)
          .fill(0)
          .map((val, index) => `${index}`),
      ];

      const reviewsInMonth = newReviews.filter((review) => {
        const submissionDate = new Date(review.submissionTime);
        return (
          submissionDate.getFullYear() === newSelectedYear &&
          submissionDate.getMonth() === months.indexOf(newSelectedMonth)
        );
      });

      const reviewsByDay = reviewsInMonth.map((review) => new Date(review.submissionTime).getDate());

      newSelectedReviews = reviewsByDay.reduce((reviewsByDay, newReviewDay) => {
        reviewsByDay[newReviewDay - 1] += 1;
        return reviewsByDay;
      }, Array(daysInMonth).fill(0));

      newLabel = `All reviews in ${newSelectedMonth}, ${newSelectedYear} (${reviewsByDay.length})`;
    }

    setReviews(newReviews);
    setLabel(newLabel);
    setLabels(newLabels);
    setTimeFrame(newTimeFrame);
    setSelectedMonth(newSelectedMonth);
    setSelectedYear(newSelectedYear);
    setSelectedReviews(newSelectedReviews);
    setLoading(false);
  };

  const backGroundColors = ['rgba(255, 206, 86, 0.6)'];

  const data = {
    labels,
    datasets: [
      {
        label,
        backgroundColor: backGroundColors,
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        data: selectedReviews,
      },
    ],
  };

  return (
    <>
      <h3 className="font-blue">Reviews Over Time</h3>
      {loading ? (
        <Spinner animation="border" style={{ margin: '1rem auto' }} />
      ) : (
        <div>
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <Dropdown>
              <Dropdown.Toggle>Timeframe</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                <Dropdown.Item onClick={() => applyFilters(reviews, 'All Time', selectedMonth, selectedYear)}>
                  All Time
                </Dropdown.Item>
                <Dropdown.Item onClick={() => applyFilters(reviews, 'Yearly', selectedMonth, selectedYear)}>
                  Yearly
                </Dropdown.Item>
                <Dropdown.Item onClick={() => applyFilters(reviews, 'Monthly', selectedMonth, selectedYear)}>
                  Monthly
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle>Year</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                {[
                  ...Array(latestReviewYear - earliestReviewYear + 1)
                    .fill(0)
                    .map((val, index) => `${earliestReviewYear + index}`),
                ].map((year) => (
                  <Dropdown.Item
                    key={year}
                    onClick={() => applyFilters(reviews, timeFrame, selectedMonth, parseInt(year))}
                  >
                    {year}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle>Month</Dropdown.Toggle>
              <Dropdown.Menu style={{ maxHeight: '10rem', overflowY: 'scroll' }}>
                {months.map((month) => (
                  <Dropdown.Item key={month} onClick={() => applyFilters(reviews, 'Monthly', month, selectedYear)}>
                    {month}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <Bar
            data={data}
            options={{
              responsive: true,
              onClick: (e, element) => {
                if (element && element[0]) {
                  const index: number = element[0].index;
                  if (timeFrame === 'All Time') {
                    applyFilters(reviews, 'Yearly', selectedMonth, earliestReviewYear + index);
                  } else if (timeFrame === 'Yearly') {
                    applyFilters(reviews, 'Monthly', months[index], selectedYear);
                  }
                }
              },
            }}
          />
        </div>
      )}
    </>
  );
};

export default ReviewsTimeline;
