import React from 'react';
import { useState, useEffect } from 'react';
import Styled from 'styled-components';

import { getBrandList } from '../../api';
import BrandType from '../../types/BrandType';

import Chart from 'react-chartjs-2';
import Button from 'react-bootstrap/Button';
import LoadingImage from '../Components/LoadingImage';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const BrandVisualization = (): JSX.Element => {
  //pagination
  const [allBrands, setAllBrands] = useState<BrandType[]>([]);
  const [brands, setBrands] = useState<BrandType[]>([]);
  const [page, setPage] = useState<number>(1);
  const itemsPerPage = 25;

  const nextPage = () => {
    setPage(page + 1);
  };

  const lastPage = () => {
    setPage(page - 1);
  };

  //pagination
  useEffect(() => {
    const getBrands = async () => {
      const brands = await getBrandList();
      const brandList = brands;
      brandList.sort((x, y) => y.numProducts - x.numProducts);
      console.log(brandList);
      setAllBrands(brandList);
      setBrands(brandList.slice(0, itemsPerPage));
    };
    getBrands();
  }, []);

  // pagination
  useEffect(() => {
    setBrands(allBrands.slice((page - 1) * 25, page * 25));
  }, [page]);

  //spinner
  let visualization = null;
  if (brands.length > 0) {
    visualization = <BrandBarGraph brands={brands} />;
  } else {
    visualization = <LoadingImage />;
  }

  return (
    <div>
      <Header>
        <h3 className="font-blue"> Brands by average reviews and number of products </h3>
        <div>
          <Button variant="light" onClick={lastPage} disabled={page === 1}>
            {'<'}
          </Button>
          <Button variant="light" onClick={nextPage} disabled={page === Math.ceil(allBrands.length / itemsPerPage)}>
            {'>'}
          </Button>
        </div>
      </Header>
      {visualization}
    </div>
  );
};

const BrandBarGraph = ({ brands }: { brands: BrandType[] }): JSX.Element => {
  const data = {
    labels: brands.map((a) => a.name),
    datasets: [
      {
        type: 'bar' as const,
        label: 'Number of Products',
        backgroundColor: 'rgb(54, 162, 235, .7)',
        borderColor: 'rgb(54, 162, 235)',
        borderWidth: 1,
        data: brands.map((a) => a.numProducts),
        yAxisID: 'number',
      },
      {
        type: 'line' as const,
        label: 'Average Review',
        backgroundColor: 'rgb(200, 162, 235, .7)',
        borderColor: 'rgb(200, 162, 235)',
        borderWidth: 1,
        data: brands.map((a) => a.avgRating),
        yAxisID: 'y',
      },
    ],
    responsive: true,
    maintainAspectRatio: false,
    beginAtZero: true,
  };

  return (
    <Chart
      type="bar"
      data={data}
      options={{
        indexAxis: 'x',
        responsive: true,
        scales: {
          y: {
            position: 'right',
            max: 5.0,
          },
          number: {
            position: 'left',
          },
        },
      }}
    />
  );
};

export default BrandVisualization;
