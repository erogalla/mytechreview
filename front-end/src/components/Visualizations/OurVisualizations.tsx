import React from 'react';
import BrandVisualization from './BrandBarGraph';
import ReviewsTimeline from './ReviewsTimeline';
import ProductVizualization from './ProductsChart';

const Visualizations = (): JSX.Element => {
  return (
    <>
      <h1 className="model-title">Our Visualizations</h1>
      <p className="model-intro">Check out our visualizations of our site.</p>
      <div style={{ width: '50%', display: 'flex', flexDirection: 'column', margin: 'auto', gap: '5rem' }}>
        <ProductVizualization />
        <ReviewsTimeline />
        <BrandVisualization />
      </div>
    </>
  );
};

export default Visualizations;
