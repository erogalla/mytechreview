// Functions
import React from 'react';

// Components
import Search from '../Search/Search';
import Image from 'react-bootstrap/Image';
import YouTube from 'react-youtube';
import './SplashStyle.css';
import '../../assets/main.css';

import ProductsImg from './products.jpg';
import BrandsImg from './brands.jpg';
import ReviewsImg from './reviews.jpg';

const Splash = (): JSX.Element => {
  return (
    <>
      <div className="splash">
        <div className="splash-content">
          <h1 className="title">My Tech Review</h1>
          <p className="intro">
            Looking for a new technology product for your school, work, or personal use? My Tech Review provides
            everything you need to know to help you find the product that best suites your needs!
          </p>
          <a href="##models" className="button">
            Get Started
          </a>
        </div>
      </div>
      <section id="#models">
        <div id="product_section" className="model">
          <Image src={ProductsImg} className="left-img" />
          <div className="right-content">
            <h2 className="subtitle">Products</h2>
            <p className="model-txt">Look for your specific product</p>
            <a href="#/products" className="model-button">
              See Products
            </a>
          </div>
        </div>
        <div id="brand_section" className="model">
          <Image src={BrandsImg} className="right-img" />
          <div className="left-content">
            <h2 className="subtitle">Brands</h2>
            <p className="model-txt">Look for your product by brand</p>
            <a href="#/brands" className="model-button">
              See Brands
            </a>
          </div>
        </div>
        <div id="review_sectiond" className="model">
          <Image src={ReviewsImg} className="left-img" />
          <div className="right-content">
            <h2 className="subtitle">Reviews</h2>
            <p className="model-txt">Look for your product by reviews</p>
            <a href="#/products" className="model-button">
              See Reviews
            </a>
          </div>
        </div>
        <div id="search_section" className="search">
          <h2 className="search-subtitle">Can&apos;t find what you are looking for?</h2>
          <p className="search-txt">Search for a product, brand, or review</p>
          <Search to={'/#/search'} />
        </div>
        <div id="demo" className="search">
          <h2 className="search-subtitle">My Tech Review Demo</h2>
          <p style={{ alignContent: 'center' }}>
            <YouTube videoId={'UHwOVKghWs4'} />
          </p>
        </div>
      </section>
    </>
  );
};

export default Splash;
