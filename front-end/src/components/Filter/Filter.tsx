// Functions
import React from 'react';
import { useLocation } from 'react-router-dom';
import { queryStringsByPrefix, searchWithQueryString, searchWithoutQueryString } from '../../utils';

import Multiselect from 'multiselect-react-dropdown';

type SearchParameters = {
  filterOptions: string[];
  attribute: string;
  text: string;
};

const Filter = ({ filterOptions, attribute, text }: SearchParameters): JSX.Element => {
  const location = useLocation();

  const selectedFilters = queryStringsByPrefix(location.search, attribute).filter((selectedFilter) =>
    filterOptions.includes(selectedFilter),
  );

  // eslint-disable-next-line
  const onSelect = (selectedList: any[], selectedItem: any): void => {
    let newSearch = location.search;
    if (attribute === 'priceRange' || attribute === 'numProducts' || attribute === 'numRatings') {
      newSearch = searchWithoutQueryString(newSearch, attribute);
    }
    newSearch = searchWithQueryString(newSearch, attribute, selectedItem.key);
    newSearch = searchWithoutQueryString(newSearch, 'paginate');
    newSearch = searchWithoutQueryString(newSearch, 'page');
    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  // eslint-disable-next-line
  const onRemove = (selectedList: any[], removedItem: any) => {
    let newSearch = searchWithoutQueryString(location.search, attribute, removedItem.key);
    newSearch = searchWithoutQueryString(newSearch, 'paginate');
    newSearch = searchWithoutQueryString(newSearch, 'page');
    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  const multiselectStyle = {
    multiselectContainer: {
      width: '150px',
    },
    searchBox: {
      width: '150px',
      backgroundColor: 'white',
      // height: '30px',
    },
    chips: {
      background: 'rgb(100, 168, 184)',
      maxWidth: '130px',
    },
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
      <Multiselect
        options={filterOptions.map((filterOption) => ({ key: filterOption }))} // Options to display in the dropdown
        selectedValues={selectedFilters.map((selectedFilter) => ({ key: selectedFilter }))}
        onSelect={onSelect} // Function will trigger on select event
        onRemove={onRemove} // Function will trigger on remove event
        displayValue="key" // Property name to display in the dropdown options
        showCheckbox={true}
        placeholder={text}
        closeOnSelect={false}
        showArrow={true}
        style={multiselectStyle}
        id={text}
      />
    </div>
  );
};

export default Filter;
