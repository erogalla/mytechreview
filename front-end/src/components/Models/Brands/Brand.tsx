// Functions
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

// Components
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';
import LoadingImage from '../../Components/LoadingImage';
import ReviewsChart from '../../Components/ReviewsChart';
import Table from 'react-bootstrap/Table';

// API
import { getBrand, getProductsByBrand, getReviewsByBrandId } from '../../../api';

// Types
import BrandType from '../../../types/BrandType';
import ProductType from '../../../types/ProductType';

import '../ModelStyle.css';

const Brand = (): JSX.Element => {
  const { brandID } = useParams<{ brandID: string }>();
  const [brand, setBrand] = useState<BrandType>();
  const [products, setProducts] = useState<ProductType[]>([]);

  useEffect(() => {
    const findBrand = async () => {
      const brand = await getBrand(brandID);
      console.log(brand);
      const productList = await getProductsByBrand(brandID);
      console.log(productList);
      setBrand(brand);
      setProducts(productList.slice(0, 5));
    };
    findBrand();
  }, []);

  return BrandCard(brand, products);
};

const BrandCard = (brand?: BrandType, products?: ProductType[]) => {
  const [stars, setStars] = useState<number[]>([]);

  console.log(brand);

  // get Reviews By Brand
  useEffect(() => {
    const findReviews = async () => {
      if (!brand || !products) return;
      const reviews = await getReviewsByBrandId(brand.id);
      const reviewStars = reviews.map((review) => review.numStars);
      console.log(reviewStars);
      setStars(reviewStars);
    };
    findReviews();
  }, [brand, products]);

  const fetchLink = (reviewId: number) => {
    window.location.assign('#/review/' + reviewId);
  };

  return (
    <div>
      {brand && products ? (
        <>
          <h1 className="instance-title">{brand.name}</h1>
          <div className="instance-row">
            <div className="instance-image">{brand.img && <LoadingImage src={brand.img} />}</div>
            <Card className="instance-card">
              <Card.Title className="instance-card-title">About the Brand</Card.Title>
              <Card.Text>
                {brand.description} <br /> <br />
                CEO: {brand.nameOfCEO} <br />
                Founded in: {brand.founded} <br />
                Heaquarters: {brand.country} <br />
                {brand.avgRating} / 5 rating <br />
                {brand.num_employees} employees <br />
                {brand.numProducts} products <br />
                Revenue: ${brand.revenue} <br />
                {stars.length} reviews <br />
              </Card.Text>
            </Card>
          </div>
          <div className="instance-row">
            {stars.length !== 0 ? (
              <div className="instance-chart">
                <ReviewsChart reviews={stars} />
              </div>
            ) : (
              <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1rem' }}>
                <h2>Reviews Loading...</h2>
                <Spinner animation="border" style={{ margin: 'auto' }} />
              </div>
            )}
            <div className="instance-list">
              <h2 className="list-title"> Products </h2>
              <Table striped bordered hover>
                <tbody style={{ cursor: 'pointer' }}>
                  {products.map((product) => (
                    <tr key={String(product.id)} id={String(product.id)} onClick={() => fetchLink(product.id)}>
                      <td>{product.name}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </>
      ) : (
        <Card.Body>
          <Card.Title>No Brand available.</Card.Title>
        </Card.Body>
      )}
    </div>
  );
};

export default Brand;
