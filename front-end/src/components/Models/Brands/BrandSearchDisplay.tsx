import React from 'react';

import BrandType from '../../../types/BrandType';

import { Link } from 'react-router-dom';

import Highlight from '../../Search/Highlight';

type BrandResultProps = {
  brand: BrandType;
  search: string[];
};

const BrandResult = ({ brand, search }: BrandResultProps) => {
  return (
    <div>
      <h5>
        {' '}
        <Link style={{ textDecoration: 'none' }} to={`/brand/${brand.id}`}>
          <Highlight search={search} display={brand.name} id={brand.id + '-name-'} />
        </Link>{' '}
      </h5>
      <p>
        {brand.country !== null ? (
          <>
            {' '}
            {'Country: '}
            <Highlight search={search} display={brand.country} id={brand.id + '-country-'} />{' '}
          </>
        ) : null}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {brand.founded !== null ? (
          <>
            {' '}
            {'Founded: '}
            <Highlight search={search} display={String(brand.founded)} id={brand.id + '-founded-'} />{' '}
          </>
        ) : null}
      </p>
      <p>
        {' '}
        <Highlight search={search} display={brand.description} id={brand.id + '-desc-'} />{' '}
      </p>
    </div>
  );
};

type SearchDisplayProps = {
  search: string;
  brandList: BrandType[];
};

const BrandSearchDisplay = ({ search, brandList }: SearchDisplayProps): JSX.Element => {
  if (search == '') {
    return (
      <>
        <p> Make a search to get started. </p>
      </>
    );
  } else if (brandList.length == 0) {
    return (
      <>
        <p> No results found. </p>
      </>
    );
  } else {
    const searchArray = search.split(' ');
    return (
      <>
        {brandList.map((singleBrand) => (
          <BrandResult key={singleBrand.id} brand={singleBrand} search={searchArray} />
        ))}
      </>
    );
  }
};

export default BrandSearchDisplay;
