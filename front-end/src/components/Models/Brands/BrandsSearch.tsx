import React from 'react';
import Styled from 'styled-components';
import { useLocation } from 'react-router';
import { useState, useEffect } from 'react';

import PageNavigation from '../../Components/PageNavigation';

// Components
import Search from '../../Search/Search';
import BrandSearchDisplay from './BrandSearchDisplay';
import TableSearchNav from '../../Search/TableSearchNav';

// Typescript types
import BrandType from '../../../types/BrandType';

// API
import { getBrandBySearch } from '../../../api';

import '../ModelStyle.css';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const BrandsSearch = (): JSX.Element => {
  const { search } = useLocation();
  const queryParams = React.useMemo(() => new URLSearchParams(search), [search]);
  const searchTerm = queryParams.get('search');
  let pageNumber = Number(queryParams.get('page'));
  pageNumber = pageNumber !== NaN && pageNumber > 0 ? pageNumber : 1;

  const [brandList, setBrandList] = useState<BrandType[]>([]);
  const [totalCount, setTotalCount] = useState<number>(0);
  const itemsPerPage = 20;

  useEffect(() => {
    const getBrands = async () => {
      const brands = await getBrandBySearch(searchTerm !== null ? searchTerm : '', pageNumber);

      setBrandList(brands.data);
      setTotalCount(brands.totalCount);
    };
    getBrands();
  }, [search]);

  return (
    <body className="model-style">
      <div className="model-content">
        <h1 className="model-title">Brands</h1>
        <p className="model-intro">
          Look through our brands based on average rating, number of products, number of reviews, revenue, and much more
          to find the right product for you!
        </p>
        <TableSearchNav activeKey={'/#/brands-search'} url={'/#/brands'} />
        <h2 className="search-title">Search Brands</h2>
        <Header>
          {totalCount > itemsPerPage && (
            <PageNavigation pageNumber={pageNumber} maxItems={totalCount} lastPage={Math.floor(totalCount / 20)} />
          )}
        </Header>
        <p className="search-intro">Can&apos;t find the exact brand you are looking for?</p>
        <Search to="/#/brands-search" search={searchTerm !== null ? searchTerm : ''} />

        <br />
        <BrandSearchDisplay search={searchTerm !== null ? searchTerm : ''} brandList={brandList} />
      </div>
    </body>
  );
};

export default BrandsSearch;
