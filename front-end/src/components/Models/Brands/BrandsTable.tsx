// Functions
import React from 'react';
import {
  cleanSearch,
  queryStringsByPrefix,
  searchContains,
  searchWithQueryString,
  searchWithoutQueryString,
} from '../../../utils';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

// Components
import Spinner from 'react-bootstrap/Spinner';
import PageNavigation from '../../Components/PageNavigation';

import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import TableSearchNav from '../../Search/TableSearchNav';
import Filter from '../../Filter/Filter';
import { Row, Col, Container } from 'react-bootstrap';
import Multiselect from 'multiselect-react-dropdown';

// Data
import { getBrandList, getPaginatedBrandList } from '../../../api';
import { BrandFilterOptions } from '../../Filter/FilterOptions';

// Types
import BrandType from '../../../types/BrandType';

import '../ModelStyle.css';

const BrandsTable = (): JSX.Element => {
  const [brandsData, setBrands] = useState<BrandType[]>([]);
  const [searching, setSearching] = useState(true);
  const [pageNumber, setPageNumber] = useState(0);
  const [lastPage, setLastPage] = useState(0);
  const [brandsLength, setBrandsLength] = useState(0);

  const location = useLocation();

  const attributes = ['avgRating', 'numRatings', 'numProducts', 'country', 'founded'];
  const attributesText = ['Rating', 'Number Reviews', 'Number Products', 'Country', 'Foundation Year'];
  // eslint-disable-next-line
  const sortOptions: any = {
    'Name: (A-Z)': [['sort', 'name']],
    'Name: (Z-A)': [
      ['sort', 'name'],
      ['desc', 'true'],
    ],
    'Rating: (Asc.)': [['sort', 'avgRating']],
    'Rating: (Desc.)': [
      ['sort', 'avgRating'],
      ['desc', 'true'],
    ],
    '# of Products: (Asc.)': [['sort', 'numProducts']],
    '# of Products: (Desc.)': [
      ['sort', 'numProducts'],
      ['desc', 'true'],
    ],
    'Revenue: (Asc.)': [['sort', 'revenue']],
    'Revenue: (Desc.)': [
      ['sort', 'revenue'],
      ['desc', 'true'],
    ],
    'Num Ratings: (Asc.)': [['sort', 'numRatings']],
    'Num Ratings: (Desc.)': [
      ['sort', 'numRatings'],
      ['desc', 'true'],
    ],
  };

  const optionsObj = attributes.reduce(
    (options, attribute, attributeIdx) => ({
      ...options,
      [attribute]: BrandFilterOptions[attributeIdx],
    }),
    {
      paginate: [],
      page: [],
      sort: [],
      desc: [],
    },
  );

  // Must include paginate to ensure we recieve paginatedBrandType
  let search = cleanSearch(location.search, optionsObj);
  if (!searchContains(search, 'page')) {
    search = searchWithQueryString(search, 'paginate', 'true');
    search = searchWithQueryString(search, 'page', '1');
  }

  const rawSort = queryStringsByPrefix(search, 'sort');
  let sortKey = '';
  if (!searchContains(search, 'desc')) {
    // eslint-disable-next-line
    const map: any = {
      name: 'Name: (A-Z)',
      avgRating: 'Rating: (Asc.)',
      numProducts: '# of Products: (Asc.)',
      revenue: 'Revenue: (Asc.)',
      numRatings: 'Num Ratings: (Asc.)',
    };
    sortKey = map[rawSort[0]];
  } else {
    // eslint-disable-next-line
    const map: any = {
      name: 'Name: (Z-A)',
      avgRating: 'Rating: (Desc.)',
      numProducts: '# of Products: (Desc.)',
      revenue: 'Revenue: (Desc.)',
      numRatings: 'Num Ratings: (Desc.)',
    };
    sortKey = map[rawSort[0]];
  }

  useEffect(() => {
    const getBrands = async () => {
      let tempSearch = searchWithoutQueryString(search, 'paginate');
      tempSearch = searchWithoutQueryString(tempSearch, 'page');
      const brandsList = await getBrandList(tempSearch);
      setBrandsLength(brandsList.length);

      const paginatedBrandData = await getPaginatedBrandList(search);
      setBrands(paginatedBrandData.data);
      setPageNumber(paginatedBrandData.current);
      setLastPage(paginatedBrandData.pages);

      setSearching(false);
    };

    getBrands();
  }, []);

  // eslint-disable-next-line
  const onSelect = (selectedList: any[], selectedItem: any): void => {
    const queryStrings: string[][] = sortOptions[selectedItem.key];
    let newSearch = searchWithoutQueryString(search, 'sort');
    newSearch = searchWithoutQueryString(newSearch, 'desc');
    queryStrings.forEach((queryString) => {
      newSearch = searchWithQueryString(newSearch, queryString[0], queryString[1]);
    });
    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  // eslint-disable-next-line
  const onRemove = (selectedList: any[], removedItem: any) => {
    let newSearch = searchWithoutQueryString(search, 'sort');
    newSearch = searchWithoutQueryString(newSearch, 'desc');
    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  const multiselectStyle = {
    multiselectContainer: {
      width: '200px',
    },
    searchBox: {
      width: '200px',
      backgroundColor: 'white',
    },
    chips: {
      background: 'rgb(100, 168, 184)',
      maxWidth: '130px',
    },
  };

  type CardProps = {
    name: string;
    id: string;
    rating: number;
    numReviews: number;
    numProducts: number;
    revenue: number;
    image: string;
  };

  const BrandCard = ({ name, id, rating, numProducts, numReviews, revenue, image }: CardProps) => {
    const onClick = () => {
      window.location.href = window.location.origin + '/#/brand/' + id;
    };

    return (
      <Card id={name} className="brand-card" onClick={onClick}>
        <Card.Img className="brand-card-img" variant="top" src={image} />
        <Card.Title className="instance-card-title">{name}</Card.Title>
        <Card.Text>Rating: {rating ? rating.toPrecision(3) : '0.00'}</Card.Text>
        <Card.Text>Number of Products: {numProducts}</Card.Text>
        <Card.Text>Number of Reviews: {numReviews}</Card.Text>
        <Card.Text>Revenue: ${revenue}</Card.Text>
      </Card>
    );
  };

  return (
    <body className="model-style">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"></link>
      <div className="model-content">
        <h1 className="model-title">Brands</h1>
        <p className="model-intro">
          Look through our brands based on average rating, number of products, number of reviews, revenue, and much more
          to find the right product for you!
        </p>
        <TableSearchNav activeKey={'/#/brands'} url={'/#/brands'} />
        <Container className="justify-content-md-center">
          <Row className="justify-content-md-center">
            {attributes.map((attribute, attributeIdx) => (
              <Col key={attribute} md="auto">
                <Filter
                  attribute={attribute}
                  text={attributesText[attributeIdx]}
                  filterOptions={BrandFilterOptions[attributeIdx]}
                />
                <br />
              </Col>
            ))}
            <Col key={'sort'} md="auto">
              <Multiselect
                options={Object.keys(sortOptions).map((sortOption: string) => ({ key: sortOption }))} // Options to display in the dropdown
                selectedValues={sortKey ? [{ key: sortKey }] : []}
                onSelect={onSelect} // Function will trigger on select event
                onRemove={onRemove} // Function will trigger on remove event
                displayValue="key" // Property name to display in the dropdown options
                showCheckbox={true}
                // singleSelect={true}
                placeholder={'Sort by... '}
                closeOnSelect={false}
                showArrow={true}
                style={multiselectStyle}
                id={'sort'}
              />
            </Col>
          </Row>
          <div className="page-nav">
            <PageNavigation pageNumber={pageNumber} lastPage={lastPage} maxItems={brandsLength} />
          </div>
        </Container>
        <br />
        <CardGroup style={{ justifyContent: 'center' }} className="model-grid">
          {brandsData.map((singleBrand: BrandType) => {
            return (
              <div key={String(singleBrand.id)} id={String(singleBrand.id)}>
                {BrandCard({
                  name: singleBrand.name,
                  id: String(singleBrand.id),
                  rating: singleBrand.avgRating,
                  numProducts: singleBrand.numProducts,
                  numReviews: singleBrand.numRatings,
                  revenue: singleBrand.revenue,
                  image: singleBrand.img,
                })}
              </div>
            );
          })}
        </CardGroup>
        {searching && (
          <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
            <Spinner animation="border" style={{ margin: 'auto' }} />
          </div>
        )}
      </div>
    </body>
  );
};

export default BrandsTable;
