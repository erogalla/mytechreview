// Functions
import React, { useState, useEffect } from 'react';
import { cleanSearch, searchContains, searchWithQueryString, searchWithoutQueryString } from '../../../utils';
import { useLocation } from 'react-router-dom';

// Components
import Table from 'react-bootstrap/Table';
import Spinner from 'react-bootstrap/Spinner';
import TableSearchNav from '../../Search/TableSearchNav';
import Filter from '../../Filter/Filter';
import PageNavigation from '../../Components/PageNavigation';
import { Row, Col, Container } from 'react-bootstrap';
import '../ModelStyle.css';

// Types
import { getProductList, getPaginatedProductList } from '../../../api';

// Types
import ProductType from '../../../types/ProductType';
import { ProductFilterOptions } from '../../Filter/FilterOptions';

import '../ModelStyle.css';

const ProductsTable = (): JSX.Element => {
  const [productsData, setProducts] = useState<ProductType[]>([]);
  const [searching, setSearching] = useState(true);
  const [pageNumber, setPageNumber] = useState(1);
  const [lastPage, setLastPage] = useState(1);
  const [productLength, setProductLength] = useState(0);

  const location = useLocation();

  const attributes = ['brand', 'rating', 'ram', 'priceRange', 'screen'];
  const attributesText = ['Brand', 'Rating', 'Ram', 'Price', 'Screen Size'];
  const optionsObj = attributes.reduce(
    (options, attribute, attributeIdx) => ({
      ...options,
      [attribute]: ProductFilterOptions[attributeIdx],
    }),
    {
      paginate: [],
      page: [],
      sort: [],
      desc: [],
    },
  );

  // Must include paginate to ensure we recieve paginatedProductType
  let search = cleanSearch(location.search, optionsObj);
  if (!searchContains(search, 'page')) {
    search = searchWithQueryString(search, 'paginate', 'true');
    search = searchWithQueryString(search, 'page', '1');
  }

  useEffect(() => {
    const getProducts = async () => {
      let tempSearch = searchWithoutQueryString(search, 'paginate');
      tempSearch = searchWithoutQueryString(tempSearch, 'page');
      const products = await getProductList(tempSearch);
      setProductLength(products.length);

      const paginatedProductData = await getPaginatedProductList(search);
      setProducts(paginatedProductData.data);
      setLastPage(paginatedProductData.pages);
      setPageNumber(paginatedProductData.current);

      setSearching(false);
    };
    getProducts();
  }, []);

  const sortData = (sortKey: keyof ProductType) => {
    const search = location.search;
    let newSearch = searchWithoutQueryString(search, 'sort');
    newSearch = searchWithoutQueryString(newSearch, 'paginate');
    newSearch = searchWithoutQueryString(newSearch, 'page');
    newSearch = searchWithoutQueryString(newSearch, 'desc');
    newSearch = searchWithQueryString(newSearch, 'sort', `${sortKey}`);
    if (searchContains(search, 'sort', sortKey) && !searchContains(search, 'desc')) {
      newSearch = searchWithQueryString(newSearch, 'desc', 'true');
    }

    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  const fetchLink = (productId: number) => {
    window.location.assign('#/product/' + productId);
  };

  return (
    <body className="model-style">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"></link>
      <div className="model-content">
        <h1 className="model-title">Products</h1>
        <p className="model-intro">
          Find the right product for you based on rating, price, brand, opertating system, memory, and much more!
        </p>
        <TableSearchNav activeKey={'/#/products'} url={'/#/products'} />
        <Container className="justify-content-md-center">
          <Row className="justify-content-md-center">
            {attributes.map((attribute, attributeIdx) => (
              <Col key={attribute} md="auto">
                <Filter
                  attribute={attribute}
                  text={attributesText[attributeIdx]}
                  filterOptions={ProductFilterOptions[attributeIdx]}
                />
                <br />
              </Col>
            ))}
          </Row>
          <div className="page-nav">
            <PageNavigation pageNumber={pageNumber} lastPage={lastPage} maxItems={productLength} />
          </div>
        </Container>
        <br />
        <Table striped bordered hover className="model-table">
          <thead className="font-blue">
            <tr>
              <th>
                <div onClick={() => sortData('name')} id={'Product Name'} style={{ cursor: 'pointer' }}>
                  Product Name
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('rating')} id={'Rating'} style={{ cursor: 'pointer' }}>
                  Rating
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('price')} id={'Price'} style={{ cursor: 'pointer' }}>
                  Price
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('os')} id={'Operating System'} style={{ cursor: 'pointer' }}>
                  Operating System
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('memory')} id={'Memory available'} style={{ cursor: 'pointer' }}>
                  Memory available
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
            </tr>
          </thead>
          {productsData.length !== 0 && (
            <tbody style={{ cursor: 'pointer' }}>
              {productsData.map((singleProduct) => (
                <tr
                  key={String(singleProduct.id)}
                  id={String(singleProduct.id)}
                  onClick={() => fetchLink(singleProduct.id)}
                >
                  <td>{singleProduct.name}</td>
                  <td>{singleProduct.rating.toPrecision(3)}</td>
                  <td>${singleProduct.price}</td>
                  <td>{singleProduct.os}</td>
                  <td>{singleProduct.memory}GB</td>
                </tr>
              ))}
            </tbody>
          )}
        </Table>
        {searching && (
          <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
            <Spinner animation="border" style={{ margin: 'auto' }} />
          </div>
        )}
      </div>
    </body>
  );
};

export default ProductsTable;
