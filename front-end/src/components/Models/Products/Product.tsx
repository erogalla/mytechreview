// Functions
import React from 'react';
import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';

// Components
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';
import LoadingImage from '../../Components/LoadingImage';
import ReviewsChart from '../../Components/ReviewsChart';
import Table from 'react-bootstrap/Table';

// API
import { getProduct, getBrand, getReviewsByProductId } from '../../../api';

// Types
import BrandType from '../../../types/BrandType';
import ProductType from '../../../types/ProductType';
import ReviewType from '../../../types/ReviewType';

import '../ModelStyle.css';

const Product = (): JSX.Element => {
  const { productId } = useParams<{ productId: string }>();
  const [brand, setBrand] = useState<BrandType>();
  const [product, setProduct] = useState<ProductType>();

  // get the product given the ID
  useEffect(() => {
    const findProduct = async () => {
      const product = await getProduct(productId);
      const brand = await getBrand(`${product.brandId}`);
      setBrand(brand);
      setProduct(product);
    };
    findProduct();
  }, []);

  return ProductCard(product, brand);
};

const ProductCard = (product?: ProductType, brand?: BrandType) => {
  const [stars, setStars] = useState<number[]>([]);
  const [shownReviews, setShownReviews] = useState<ReviewType[]>([]);

  // get Reviews By product id
  useEffect(() => {
    const findReviews = async () => {
      if (!product) return;
      const reviews = await getReviewsByProductId(product.id);
      const reviewStars = reviews.map((review) => review.numStars);
      setShownReviews(reviews.sort((a, b) => b.numHelpful - a.numHelpful).slice(0, 5));
      setStars(reviewStars);
    };
    findReviews();
  }, [product]);

  const fetchLink = (reviewId: number) => {
    window.location.assign('#/review/' + reviewId);
  };

  return (
    <div>
      {product && brand ? (
        <>
          <h1 className="instance-title">{product.name}</h1>
          <div className="instance-row">
            <div className="instance-image">
              <LoadingImage src={product.image} />
            </div>
            <Card className="instance-card">
              <Card.Title className="instance-card-title">About the Product</Card.Title>
              <Card.Text>
                {product.rating} / 5 rating <br />
                Price: {product.price}$ <br />
                Screen Size: {product.screen} <br />
                Battery life: {product.batteryLife} hours <br />
                OS: {product.os} <br />
                RAM: {product.ram} <br />
                Memory: {product.memory} GB <br />
                Brand: <Link to={`/brand/${product.brandId}`}> {brand.name} </Link> <br />
                {stars.length} reviews <br />
              </Card.Text>
            </Card>
          </div>
          <br />
          <div className="instance-row">
            {stars.length !== 0 ? (
              <div className="instance-chart">
                <ReviewsChart reviews={stars} />
              </div>
            ) : (
              <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1rem' }}>
                <h2>Reviews Loading...</h2>
                <Spinner animation="border" style={{ margin: 'auto' }} />
              </div>
            )}
            <div className="instance-list">
              <h2 className="list-title"> Reviews </h2>
              <Table striped bordered hover>
                <tbody style={{ cursor: 'pointer' }}>
                  {shownReviews.map((singleReview) => (
                    <tr
                      key={String(singleReview.id)}
                      id={String(singleReview.id)}
                      onClick={() => fetchLink(singleReview.id)}
                    >
                      <td>
                        {singleReview.username} {singleReview.numStars}/5 - {singleReview.title}
                        <p style={{ fontSize: '0.7rem', marginBottom: '0' }}>
                          {singleReview.numHelpful || 0} found this helpful.
                        </p>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
              {/* </div> */}
            </div>
          </div>
        </>
      ) : (
        <Card.Body>
          <Card.Title>No Product available.</Card.Title>
        </Card.Body>
      )}
      <br />
    </div>
  );
};

export default Product;
