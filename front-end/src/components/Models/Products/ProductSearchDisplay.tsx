import React from 'react';

import ProductType from '../../../types/ProductType';

import { Link } from 'react-router-dom';

import Highlight from '../../Search/Highlight';

type ProductResultProps = {
  product: ProductType;
  search: string[];
};

const ProductResult = ({ product, search }: ProductResultProps) => {
  return (
    <div>
      <h5>
        {' '}
        <Link style={{ textDecoration: 'none' }} to={`/product/${product.id}`}>
          <Highlight search={search} display={product.name} id={product.id + '-name-'} />
        </Link>{' '}
      </h5>
      <p>
        {product.price !== null ? (
          <>
            {' '}
            {'Price: '}
            <Highlight search={search} display={product.price + ''} id={product.id + '-price-'} />{' '}
          </>
        ) : null}
      </p>
      <p>
        {product.memory !== null ? (
          <>
            {' '}
            {'Memory: '}
            <Highlight search={search} display={product.memory + ''} id={product.id + '-memory-'} />
            {'GB'}
          </>
        ) : null}
      </p>
    </div>
  );
};

type SearchDisplayProps = {
  search: string;
  productList: ProductType[];
};

const ProductSearchDisplay = ({ search, productList }: SearchDisplayProps): JSX.Element => {
  if (search == '') {
    return (
      <>
        <p> Make a search to get started. </p>
      </>
    );
  } else if (productList.length == 0) {
    return (
      <>
        <p> No results found. </p>
      </>
    );
  } else {
    const searchArray = search.split(' ');
    return (
      <>
        {productList.map((singleProduct) => (
          <ProductResult key={singleProduct.id} product={singleProduct} search={searchArray} />
        ))}
      </>
    );
  }
};

export default ProductSearchDisplay;
