import React from 'react';
import Styled from 'styled-components';
import { useLocation } from 'react-router';
import { useState, useEffect } from 'react';

// Components
import Search from '../../Search/Search';
import PageNavigation from '../../Components/PageNavigation';
import ProductSearchDisplay from './ProductSearchDisplay';
import TableSearchNav from '../../Search/TableSearchNav';

// Typescript types
import ProductType from '../../../types/ProductType';

// API
import { getProductsBySearch } from '../../../api';

import '../ModelStyle.css';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const ProductsSearch = (): JSX.Element => {
  const { search } = useLocation();
  const queryParams = React.useMemo(() => new URLSearchParams(search), [search]);
  const searchTerm = queryParams.get('search');
  let pageNumber = Number(queryParams.get('page'));
  pageNumber = pageNumber !== NaN && pageNumber > 0 ? pageNumber : 1;

  const [productList, setProductList] = useState<ProductType[]>([]);
  const [totalCount, setTotalCount] = useState<number>(0);
  const itemsPerPage = 20;

  useEffect(() => {
    const getProducts = async () => {
      const products = await getProductsBySearch(searchTerm !== null ? searchTerm : '', pageNumber);
      setProductList(products.data);
      setTotalCount(products.totalCount);
    };
    getProducts();
  }, [search]);

  return (
    <body className="model-style">
      <div className="model-content">
        <h1 className="model-title">Products</h1>
        <p className="model-intro">
          Find the right product for you based on rating, price, brand, opertating system, memory, and much more!
        </p>
        <TableSearchNav activeKey={'/#/products-search'} url={'/#/products'} />
        <h2 className="search-title">Search Products</h2>
        <Header>
          {totalCount > itemsPerPage && (
            <PageNavigation pageNumber={pageNumber} maxItems={totalCount} lastPage={Math.floor(totalCount / 20)} />
          )}
        </Header>
        <p className="search-intro">Can&apos;t find the exact product you are looking for?</p>
        <Search to="/#/products-search" search={searchTerm !== null ? searchTerm : ''} />

        <br />
        <ProductSearchDisplay search={searchTerm !== null ? searchTerm : ''} productList={productList} />
      </div>
    </body>
  );
};

export default ProductsSearch;
