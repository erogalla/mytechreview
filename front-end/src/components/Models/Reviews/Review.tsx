// Functions
import React from 'react';
import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';

// Components
import Card from 'react-bootstrap/Card';
import { Row, Col } from 'react-bootstrap';

// API
import { getProduct, getReview, getReviewsByProductId } from '../../../api';

// Types
import ReviewType from '../../../types/ReviewType';
import ProductType from '../../../types/ProductType';
import Spinner from 'react-bootstrap/Spinner';
import LoadingImage from '../../Components/LoadingImage';
import ReviewsChart from '../../Components/ReviewsChart';

import '../ModelStyle.css';

const Review = (): JSX.Element => {
  const { reviewId } = useParams<{ reviewId: string }>();
  const [review, setReview] = useState<ReviewType>();

  // get the review given the ID
  useEffect(() => {
    const findReview = async () => {
      const foundReview = await getReview(reviewId);
      setReview(foundReview);
    };
    findReview();
  }, []);

  return ReviewCard(review);
};

const ReviewCard = (review?: ReviewType) => {
  const [product, setProduct] = useState<ProductType>();
  const [stars, setStars] = useState<number[]>([]);

  // get Reviews By product id
  useEffect(() => {
    const findReviews = async () => {
      if (!review) return;
      const reviews = await getReviewsByProductId(review.productId);
      const reviewStars = reviews.map((review) => review.numStars);
      setStars(reviewStars);
    };
    const findProduct = async () => {
      if (!review) return;
      const product = await getProduct(`${review.productId}`);
      setProduct(product);
    };
    findProduct();
    findProduct();
    findReviews();
  }, [review]);

  const dateOptions: Intl.DateTimeFormatOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  return (
    <div>
      {review ? (
        <>
          <div className="instance-row">
            <div className="instance-image">
              <LoadingImage src={product?.image} />
            </div>
          </div>
          <Card className="instance-review-card">
            <Card.Header as="h4" className="instance-review-title">
              {review.title}
            </Card.Header>
            <Row className="instance-review-row">
              <Col className="instance-col">
                {review.numStars} / 5 rating <br />
                Reviewed by {review.username} <br />
                Reviewed on {new Date(review.submissionTime).toLocaleDateString('en-US', dateOptions)} <br />
                Product: <Link to={`/product/${review.productId}`}>{review.product}</Link> <br />
                Product Brand: <Link to={`/brand/${review.brandId}`}>{review.prodBrand}</Link> <br /> <br />
                {review.numHelpful} people found this helpful <br />
                {review.numUnhelpful} people found this unhelpful <br />
                {review.numValue != -1 && review.numQuality != -1 && review.numEaseOfUse != -1 && (
                  <>
                    Value: {review.numValue} <br />
                    Quality: {review.numQuality} <br />
                    Ease of Use: {review.numEaseOfUse} <br />
                  </>
                )}{' '}
                <br />
                Number of reviews for this product: {stars.length}
              </Col>
              <Col>&quot;{review.description}&quot;</Col>
            </Row>
          </Card>
          {stars.length !== 0 ? (
            <div className="instance-row instance-chart">
              <ReviewsChart reviews={stars} thisReview={review.numStars} />
            </div>
          ) : (
            <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '1rem' }}>
              <h2>Reviews Loading...</h2>
              <Spinner animation="border" style={{ margin: 'auto' }} />
            </div>
          )}
        </>
      ) : (
        <Card.Body>
          <Card.Title>No Review available.</Card.Title>
        </Card.Body>
      )}
    </div>
  );
};

export default Review;
