import React from 'react';

import ReviewType from '../../../types/ReviewType';

import { Link } from 'react-router-dom';

import Highlight from '../../Search/Highlight';

type ReviewResultProps = {
  review: ReviewType;
  search: string[];
};

const ReviewResult = ({ review, search }: ReviewResultProps) => {
  return (
    <div>
      <h5>
        {' '}
        <Link style={{ textDecoration: 'none' }} to={`/review/${review.id}`}>
          <Highlight search={search} display={review.username + ' - ' + review.numStars} id={review.id + '-name-'} />{' '}
          Stars: <Highlight search={search} display={review.title} id={review.id + '-title-'} />{' '}
        </Link>{' '}
      </h5>
      <p>
        {' '}
        <Highlight search={search} display={review.description} id={review.id + '-desc-'} />{' '}
      </p>
    </div>
  );
};

type SearchDisplayProps = {
  search: string;
  reviewList: ReviewType[];
};

const ReviewSearchDisplay = ({ search, reviewList }: SearchDisplayProps): JSX.Element => {
  if (search == '') {
    return (
      <>
        <p> Make a search to get started. </p>
      </>
    );
  } else if (reviewList.length == 0) {
    return (
      <>
        <p> No results found. </p>
      </>
    );
  } else {
    const searchArray = search.split(' ');
    return (
      <>
        {reviewList.map((singleReview) => (
          <ReviewResult key={singleReview.id} review={singleReview} search={searchArray} />
        ))}
      </>
    );
  }
};

export default ReviewSearchDisplay;
