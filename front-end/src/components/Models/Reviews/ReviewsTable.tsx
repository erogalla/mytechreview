// Functions
import React, { useState, useEffect } from 'react';
import { cleanSearch, searchContains, searchWithQueryString, searchWithoutQueryString } from '../../../utils';
import { useLocation } from 'react-router-dom';

// Components
import Table from 'react-bootstrap/Table';
import Spinner from 'react-bootstrap/Spinner';
import PageNavigation from '../../Components/PageNavigation';
import TableSearchNav from '../../Search/TableSearchNav';

// Types
import Filter from '../../Filter/Filter';
import { Row, Col, Container } from 'react-bootstrap';

// Data
import { getReviewList, getPaginatedReviewList } from '../../../api';
import { ReviewFilterOptions } from '../../Filter/FilterOptions';

// Types
import ReviewType from '../../../types/ReviewType';

import '../ModelStyle.css';

const ReviewsTable = (): JSX.Element => {
  const [reviewData, setReview] = useState<ReviewType[]>([]);
  const [searching, setSearching] = useState(true);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const [lastPage, setLastPage] = useState<number>(1);
  const [reviewsLength, setReviewsLength] = useState(1);

  const location = useLocation();

  const attributes = ['rating', 'numHelpful', 'numUnHelpful', 'product', 'prodBrand'];
  const attributesText = ['Rating', 'Number Helpful', 'Number Unhelpful', 'Product', 'Product Brand'];
  const optionsObj = attributes.reduce(
    (options, attribute, attributeIdx) => ({
      ...options,
      [attribute]: ReviewFilterOptions[attributeIdx],
    }),
    {
      paginate: [],
      page: [],
      sort: [],
      desc: [],
    },
  );

  // Must include paginate to ensure we recieve paginatedReviewType
  let search = cleanSearch(location.search, optionsObj);
  if (!searchContains(search, 'page')) {
    search = searchWithQueryString(search, 'paginate', 'true');
    search = searchWithQueryString(search, 'page', '1');
  }

  useEffect(() => {
    const getReviews = async () => {
      let tempSearch = searchWithoutQueryString(search, 'paginate');
      tempSearch = searchWithoutQueryString(tempSearch, 'page');
      const reviewList = await getReviewList(tempSearch);
      setReviewsLength(reviewList.length);

      const paginatedReviewData = await getPaginatedReviewList(search);
      setReview(paginatedReviewData.data);
      setPageNumber(paginatedReviewData.current);
      setLastPage(paginatedReviewData.pages);

      setSearching(false);
    };
    getReviews();
  }, []);

  const sortData = (sortKey: keyof ReviewType) => {
    let newSearch = searchWithoutQueryString(search, 'sort');
    newSearch = searchWithoutQueryString(newSearch, 'paginate');
    newSearch = searchWithoutQueryString(newSearch, 'page');
    newSearch = searchWithoutQueryString(newSearch, 'desc');
    newSearch = searchWithQueryString(newSearch, 'sort', `${sortKey}`);
    if (searchContains(search, 'sort', sortKey) && !searchContains(search, 'desc')) {
      newSearch = searchWithQueryString(newSearch, 'desc', 'true');
    }

    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  const fetchLink = (reviewId: number) => {
    window.location.assign('#/review/' + reviewId);
  };

  const dateOptions: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'long', day: 'numeric' };

  return (
    <body className="model-style">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"></link>
      <div className="model-content">
        <h1 className="model-title">Reviews</h1>
        <p className="model-intro">
          Look through our reviews based on rating, number of found helpful/unhelpful, date, and much more to find the
          right product for you!
        </p>
        <TableSearchNav activeKey={'/#/reviews'} url={'/#/reviews'} />
        <Container className="justify-content-md-center">
          <Row className="justify-content-md-center">
            {attributes.map((attribute, attributeIdx) => (
              <Col key={attribute} md="auto">
                <Filter
                  attribute={attribute}
                  text={attributesText[attributeIdx]}
                  filterOptions={ReviewFilterOptions[attributeIdx]}
                />
                <br />
              </Col>
            ))}
          </Row>
          <div className="page-nav">
            <PageNavigation pageNumber={pageNumber} lastPage={lastPage} maxItems={reviewsLength} />
          </div>
        </Container>
        <Table striped bordered hover className="model-table">
          <thead className="font-blue">
            <tr>
              <th>
                <div onClick={() => sortData('product')} id={'Product'} style={{ cursor: 'pointer' }}>
                  Product
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('numStars')} id={'Rating'} style={{ cursor: 'pointer' }}>
                  Rating
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('numHelpful')} id={'Found Helpful'} style={{ cursor: 'pointer' }}>
                  Found Helpful
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('username')} id={'Reviewer'} style={{ cursor: 'pointer' }}>
                  Reviewer
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
              <th>
                <div onClick={() => sortData('submissionTime')} id={'Date of Review'} style={{ cursor: 'pointer' }}>
                  Date of Review
                  <i className="fa fa-fw fa-sort"></i>
                </div>
              </th>
            </tr>
          </thead>
          <tbody style={{ cursor: 'pointer' }}>
            {reviewData.map((singleReview) => (
              <tr key={String(singleReview.id)} id={String(singleReview.id)} onClick={() => fetchLink(singleReview.id)}>
                <td>{singleReview.product}</td>
                <td>{singleReview.numStars} / 5</td>
                <td>{singleReview.numHelpful}</td>
                <td>{singleReview.username}</td>
                <td>{new Date(singleReview.submissionTime).toLocaleDateString('en-US', dateOptions)}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        {searching && (
          <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
            <Spinner animation="border" style={{ margin: 'auto' }} />
          </div>
        )}
      </div>
    </body>
  );
};

export default ReviewsTable;
