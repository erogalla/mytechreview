import React from 'react';
import Styled from 'styled-components';
import { useLocation } from 'react-router';
import { useState, useEffect } from 'react';

// Components
import Search from '../../Search/Search';
import PageNavigation from '../../Components/PageNavigation';
import ReviewSearchDisplay from './ReviewSearchDisplay';
import TableSearchNav from '../../Search/TableSearchNav';

// Typescript types
import ReviewType from '../../../types/ReviewType';

// API
import { getReviewsBySearch } from '../../../api';

import '../ModelStyle.css';

const Header = Styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 500px) {
      flex-direction: column;
    }
  `;

const ReviewsSearch = (): JSX.Element => {
  const { search } = useLocation();
  const queryParams = React.useMemo(() => new URLSearchParams(search), [search]);
  const searchTerm = queryParams.get('search');
  let pageNumber = Number(queryParams.get('page'));
  pageNumber = pageNumber !== NaN && pageNumber > 0 ? pageNumber : 1;

  const [reviewList, setReviewList] = useState<ReviewType[]>([]);
  const [totalCount, setTotalCount] = useState<number>(0);

  useEffect(() => {
    const getReviews = async () => {
      const reviews = await getReviewsBySearch(searchTerm !== null ? searchTerm : '', pageNumber);

      setReviewList(reviews.data);
      setTotalCount(reviews.totalCount);
    };
    getReviews();
  }, [search]);

  return (
    <body className="model-style">
      <div className="model-content">
        <h1 className="model-title">Reviews</h1>
        <p className="model-intro">
          Look through our reviews based on rating, number of found helpful/unhelpful, date, and much more to find the
          right product for you!
        </p>
        <TableSearchNav activeKey={'/#/reviews-search'} url={'/#/reviews'} />
        <h2 className="search-title">Search Reviews</h2>
        <Header>
          {totalCount > 20 && (
            <PageNavigation pageNumber={pageNumber} maxItems={totalCount} lastPage={Math.floor(totalCount / 20)} />
          )}
        </Header>
        <p className="search-intro">Can&apos;t find the exact review you are looking for?</p>
        <Search to="/#/reviews-search" search={searchTerm !== null ? searchTerm : ''} />

        <br />
        <ReviewSearchDisplay search={searchTerm !== null ? searchTerm : ''} reviewList={reviewList} />
      </div>
    </body>
  );
};

export default ReviewsSearch;
