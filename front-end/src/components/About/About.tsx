// Functions
import React from 'react';
import { useState, useEffect } from 'react';

// Components
import CardGroup from 'react-bootstrap/CardGroup';
import Card from 'react-bootstrap/Card';

// Data
import { fetchGitLabStats } from '../../api/GitlabStats';
import { repoAndApi, tools, data } from '../../api/AboutInfo';
import initialGitLabStats from '../../api/TeamMemberInfo';
import CommitsIcon from '../../assets/Images/CommitsIcon.png';
import IssuesIcon from '../../assets/Images/IssuesIcon.png';
import TestsIcon from '../../assets/Images/TestsIcon.jpg';

// Types
import { GitlabStatsType } from '../../types/GitlabTypes';
import { AboutInfoType } from '../../types/AboutInfoType';

import './AboutStyle.css';

const About = (): JSX.Element => {
  const [gitLabStats, setGitLabStats] = useState<GitlabStatsType[]>([]);
  const [totalTests, setTotalTests] = useState<number>(0);
  const totalCommits = gitLabStats.reduce((commitsSoFar, teamMember) => commitsSoFar + teamMember.commits, 0);
  const totalIssues = gitLabStats.reduce((issuesSoFar, teamMember) => issuesSoFar + teamMember.issues, 0);

  // runs after every render
  useEffect(() => {
    const getGitLabStats = async () => {
      const fetchedGitLabStats = await fetchGitLabStats(initialGitLabStats);
      setGitLabStats(fetchedGitLabStats);
    };
    const getNumTests = async () => {
      // const fetchedGitLabTests = await fetchGitLabTests();
      // if (fetchedGitLabTests) setTotalTests(fetchedGitLabTests.numTests);
      setTotalTests(111);
    };

    getGitLabStats();
    getNumTests();
  }, [initialGitLabStats]);

  const topRowBios = gitLabStats.slice(0, 3);
  const botRowBios = gitLabStats.slice(3, 5);

  return (
    <div style={{ textAlign: 'center' }}>
      <h1 className="font-blue">
        <br />
        About MyTechReview
      </h1>
      <p className="font-grey text-center about-us">
        MyTechReview is a website that provides information about a variety of electronic products, brands, and user
        reviews. We are dedicated to bringing expertise for you so that you can feel confident about your tech
        purchases. Whether you are buying a laptop for school, a phone for a friend, or a desktop for your home office,
        we hope to provide a helpful resource that will allow you to make decisions that best fit your needs.
      </p>
      <h2 className="font-blue">
        <br />
        Who We Are
      </h2>
      <CardGroup style={{ justifyContent: 'center' }}>
        {topRowBios.map((teamMember) => {
          return <div key={teamMember.username}>{AboutCard(teamMember)}</div>;
        })}
      </CardGroup>
      <CardGroup style={{ justifyContent: 'center' }}>
        {botRowBios.map((teamMember) => {
          return <div key={teamMember.username}>{AboutCard(teamMember)}</div>;
        })}
      </CardGroup>
      <h2 className="font-blue">
        <br />
        <br />
        Our Statistics
      </h2>
      <CardGroup style={{ justifyContent: 'center' }}>
        <div key="totalCommits">
          {StatsCard({ title: 'Total Commits:', description: totalCommits, img: CommitsIcon })}
        </div>
        <div key="totalIssues">{StatsCard({ title: 'Total Issues:', description: totalIssues, img: IssuesIcon })}</div>
        <div key="totalTests">{StatsCard({ title: 'Total Tests:', description: totalTests, img: TestsIcon })}</div>
      </CardGroup>
      <h2 className="font-blue">
        <br />
        <br />
        Data Utilized
      </h2>
      <p className="font-grey text-center about-us">
        While observing and integrating disparate data, our team noticed higher rated reviews tend to reveive more
        people who found it helpful.
      </p>
      <CardGroup style={{ justifyContent: 'center' }}>
        {data.map((data: AboutInfoType) => {
          return <div key={data.title}>{InfoCard(data)}</div>;
        })}
      </CardGroup>
      <h2 className="font-blue">
        <br />
        <br />
        Tools Utilized
      </h2>
      <CardGroup style={{ justifyContent: 'center' }}>
        {tools.map((tool: AboutInfoType) => {
          return <div key={tool.title}>{InfoCard(tool)}</div>;
        })}
      </CardGroup>
      <h2 className="font-blue">
        <br />
        <br />
        Our Repo and API
      </h2>
      <CardGroup style={{ justifyContent: 'center' }}>
        {repoAndApi.map((card: AboutInfoType) => {
          return <div key={card.title}>{InfoCard(card)}</div>;
        })}
      </CardGroup>
    </div>
  );
};

const AboutCard = (info: GitlabStatsType): JSX.Element => {
  const { name, img, role, bio, commits, issues, tests } = info;

  return (
    <Card id={name} className="bio-card" style={{ width: '18rem' }}>
      <Card.Img className="bio-card-img" variant="top" src={img} />
      <Card.Body>
        <Card.Title className="font-blue">{name}</Card.Title>
        <Card.Text className="font-blue">Role: {role}</Card.Text>
        <Card.Text>{bio}</Card.Text>
        <Card.Text>Commits: {commits}</Card.Text>
        <Card.Text>Issues: {issues}</Card.Text>
        <Card.Text>Unit Tests: {tests}</Card.Text>
      </Card.Body>
    </Card>
  );
};

const StatsCard = (info: { title: string; description: number; img: string }): JSX.Element => {
  const { title, description, img } = info;
  return (
    <Card id={title} className="stats-card" style={{ width: '18rem' }}>
      <Card.Img className="stats-card-img" variant="top" src={img} />
      <Card.Body>
        <Card.Text className="font-blue">
          {title} {description}
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

const InfoCard = (info: AboutInfoType): JSX.Element => {
  const { title, description, img, link } = info;

  const onClick = () => {
    window.location.href = link;
  };

  return (
    <Card id={title} className="info-card" onClick={onClick} style={{ width: '18rem' }}>
      <Card.Img className="info-card-img" variant="top" src={img} />
      <Card.Body>
        <Card.Title className="font-blue">{title}</Card.Title>
        <Card.Text>{description}</Card.Text>
      </Card.Body>
    </Card>
  );
};
export default About;
