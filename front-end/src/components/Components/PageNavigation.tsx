import React from 'react';
import { useLocation } from 'react-router-dom';
import { searchWithQueryString, searchWithoutQueryString } from '../../utils';

// Components
import Styled from 'styled-components';
import PaginationItem from './PaginationItem';

type PageNavigationProps = {
  pageNumber: number;
  lastPage: number;
  maxItems: number;
};

const PageNavigation = ({ pageNumber, lastPage, maxItems }: PageNavigationProps): JSX.Element => {
  const location = useLocation();

  const changePage = (newPage: number) => {
    const search = location.search;
    let newSearch = searchWithoutQueryString(search, 'page');
    newSearch = searchWithoutQueryString(newSearch, 'paginate');
    newSearch = searchWithQueryString(newSearch, 'paginate', 'true');
    newSearch = searchWithQueryString(newSearch, 'page', `${newPage}`);

    window.location.assign('#' + location.pathname + '?' + newSearch);
    window.location.reload();
  };

  const TotalItems = Styled.p`
    margin-right: 1rem;
  `;

  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        {lastPage !== 0 && (
          <>
            <TotalItems>
              {(pageNumber - 1) * 20} - {Math.min(pageNumber * 20, maxItems)}
              {pageNumber !== lastPage && ` / (${maxItems})`}
            </TotalItems>
            {pageNumber !== 1 && <PaginationItem text="1" onClick={() => changePage(1)} />}
            {pageNumber > 3 && <PaginationItem text="..." />}
            {pageNumber > 2 && <PaginationItem text={`${pageNumber - 1}`} onClick={() => changePage(pageNumber - 1)} />}
            <PaginationItem text={`${pageNumber}`} current={true} />
            {pageNumber + 1 < lastPage && (
              <PaginationItem text={`${pageNumber + 1}`} onClick={() => changePage(pageNumber + 1)} />
            )}
            {pageNumber + 2 < lastPage && <PaginationItem text="..." />}
            {pageNumber !== lastPage && <PaginationItem text={`${lastPage}`} onClick={() => changePage(lastPage)} />}
          </>
        )}
      </ul>
    </nav>
  );
};

export default PageNavigation;
