// Functions
import React from 'react';

// Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import './NavbarStyle.css';

const TechReviewNavbar = (): JSX.Element => {
  return (
    <div>
      <Navbar expand="sm" className="navbar">
        <Navbar.Brand className="nav-bar" href="#">
          <b>My Tech Review</b>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link className="nav-bar" href="#/">
              Home
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/products">
              Products
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/brands">
              Brands
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/reviews">
              Reviews
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/visualizations">
              Visualizations
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/provider-visualizations">
              Provider Visualizations
            </Nav.Link>
            <Nav.Link className="nav-bar" href="#/about">
              About
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};

export default TechReviewNavbar;
