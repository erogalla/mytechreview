import React from 'react';

type PaginationItemProps = {
  text: string;
  current?: boolean;
  onClick?: () => void;
};

const PaginationItem = ({ text, current, onClick }: PaginationItemProps): JSX.Element => {
  let style = {};
  if (current) {
    style = {
      background: '#5e5e5e',
      color: 'white',
    };
  }

  return (
    <li className="page-item">
      <a
        className="page-link"
        href=""
        onClick={(e) => {
          e.preventDefault();
          if (onClick) onClick();
        }}
        style={style}
      >
        {text}
      </a>
    </li>
  );
};

export default PaginationItem;
