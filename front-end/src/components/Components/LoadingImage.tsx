import React, { useState } from 'react';

// Components
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';

const LoadingImage = ({ src }: { src?: string }): JSX.Element => {
  const [loaded, setLoaded] = useState<boolean>(false);

  const imageContainerStyle = { height: '20rem', padding: '1rem' };

  const imageStyle = {
    maxHeight: '100%',
    'object-fit': 'scale-down',
  };

  const loadingStyle = {
    display: 'flex',
    'flex-direction': 'column',
  };

  return (
    <div style={imageContainerStyle}>
      {src && <Card.Img src={src} onLoad={() => setLoaded(true)} style={imageStyle} />}
      {loaded || (
        <div style={loadingStyle}>
          <h3 className="font-blue">Loading Image</h3>
          <Spinner animation="border" style={{ margin: '1rem auto', color: 'rgb(100, 168, 184)' }} />
        </div>
      )}
    </div>
  );
};

export default LoadingImage;
