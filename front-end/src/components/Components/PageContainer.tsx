// Functions
import React from 'react';

// Components
import TechReviewNavbar from './TechReviewNavbar';

type Props = {
  children: JSX.Element | string | (string | JSX.Element)[];
};

const PageContainer = ({ children }: Props): JSX.Element => {
  const contentStyle = {
    margin: '3rem',
  };

  return (
    <>
      <TechReviewNavbar />
      <div className="main-content" style={contentStyle}>
        {children}
      </div>
    </>
  );
};

export default PageContainer;
