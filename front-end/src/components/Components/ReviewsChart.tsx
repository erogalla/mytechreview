import React from 'react';
import { Bar } from 'react-chartjs-2';

type ReviewsChartProps = {
  reviews: number[];
  thisReview?: number;
};

const ReviewsChart = ({ reviews, thisReview }: ReviewsChartProps): JSX.Element => {
  const reviewsCount = [0, 0, 0, 0, 0];
  reviews.forEach((review) => {
    if (review >= 1 && review <= 5) {
      reviewsCount[5 - review]++;
    }
  });

  const backGroundColors = [
    'rgba(255, 206, 86, 0.6)',
    'rgba(255, 206, 86, 0.6)',
    'rgba(255, 206, 86, 0.6)',
    'rgba(255, 206, 86, 0.6)',
    'rgba(255, 206, 86, 0.6)',
  ];

  if (thisReview) {
    backGroundColors[5 - thisReview] = 'rgba(255, 195, 0, 1)';
  }

  const data = {
    labels: ['5 Stars', '4 Stars', '3 Stars', '2 Stars', '1 Star'],
    datasets: [
      {
        label: `Reviews (${reviews.length})`,
        backgroundColor: backGroundColors,
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1,
        data: reviewsCount,
      },
    ],
  };

  return (
    <Bar
      data={data}
      options={{
        indexAxis: 'y',
        responsive: true,
      }}
    />
  );
};

export default ReviewsChart;
