import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import './SearchStyle.css';

type SearchProps = {
  to: string;
  search?: string;
};

const Search = ({ to, search }: SearchProps): JSX.Element => {
  const [searchTerm, setSearchTerm] = useState<string>(search !== undefined ? search : '');

  const onSearch = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    window.location.href = window.location.origin + to + '?search=' + searchTerm;
  };

  return (
    <Form onSubmit={onSearch}>
      <div className="input-group" style={{ justifyContent: 'space-between' }}>
        <div className="form-outline" style={{ flexGrow: 1 }}>
          <Form.Control
            name="search"
            value={searchTerm}
            onChange={(ev: React.ChangeEvent<HTMLInputElement>): void => setSearchTerm(ev.target.value)}
            type="text"
            id="search"
          />
        </div>
        <Button variant="primary" type="submit">
          Search
        </Button>
      </div>
    </Form>
  );
};

export default Search;
