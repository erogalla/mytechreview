import React from 'react';

type HighlightProps = {
  search: string[];
  display: string;
  id: string;
};

const Highlight = ({ search, display, id }: HighlightProps): JSX.Element => {
  if (display == null) {
    return <></>;
  }

  const words = display.split(' ');
  const highlights = search.map((term) => term.toLowerCase());

  return (
    <>
      {words.map((word: string, idx: number) => {
        const lowerCaseWord = word.toLowerCase();
        if (
          highlights.some((highlight) => {
            return lowerCaseWord.indexOf(highlight) >= 0;
          })
        ) {
          return (
            <React.Fragment key={id + idx}>
              <mark style={{ backgroundColor: '#FFFF88', padding: '0' }}>{word}</mark>{' '}
            </React.Fragment>
          );
        }
        return word + ' ';
      })}
    </>
  );
};

export default Highlight;
