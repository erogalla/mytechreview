// Functions
import React from 'react';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

// API
import { SearchType, getSearch, emptySearch } from '../../api';

import { Link } from 'react-router-dom';
import ProductSearchDisplay from '../Models/Products/ProductSearchDisplay';
import BrandSearchDisplay from '../Models/Brands/BrandSearchDisplay';
import ReviewSearchDisplay from '../Models/Reviews/ReviewSearchDisplay';
import Search from './Search';

const GeneralSearch = (): JSX.Element => {
  const { search } = useLocation();
  const queryParams = React.useMemo(() => new URLSearchParams(search), [search]);
  const searchTerm = queryParams.get('search');

  const [searchResult, setSearchResult] = useState<SearchType>(emptySearch);
  useEffect(() => {
    const getSearchResult = async () => {
      const result = await getSearch(searchTerm !== null ? searchTerm : '');
      setSearchResult(result);
      console.log(result);
    };
    getSearchResult();
  }, [search]);

  let productDiv = null;
  if (searchResult.numProducts !== 0) {
    productDiv = (
      <div>
        <h3>
          Products
          {searchResult.numProducts > 9 && (
            <>
              {' - '}
              <Link to={'/products-search?search=' + searchTerm}>
                Click here for {searchResult.numProducts - 9} more products
              </Link>
            </>
          )}
        </h3>
        <ProductSearchDisplay search={searchTerm !== null ? searchTerm : ''} productList={searchResult.products} />
      </div>
    );
  }

  let brandDiv = null;
  if (searchResult.numBrands !== 0) {
    brandDiv = (
      <div>
        <h3>
          Brands
          {searchResult.numBrands > 9 && (
            <>
              {' - '}
              <Link to={'/brands-search?search=' + searchTerm}>
                Click here for {searchResult.numBrands - 9} more brands
              </Link>
            </>
          )}
        </h3>
        <BrandSearchDisplay search={searchTerm !== null ? searchTerm : ''} brandList={searchResult.brands} />
      </div>
    );
  }

  let reviewDiv = null;
  if (searchResult.numReviews !== 0) {
    reviewDiv = (
      <div>
        <h3>
          Reviews
          {searchResult.numReviews > 9 && (
            <>
              {' - '}
              <Link to={'/reviews-search?search=' + searchTerm}>
                Click here for {searchResult.numProducts - 9} more reviews
              </Link>
            </>
          )}
        </h3>
        <ReviewSearchDisplay search={searchTerm !== null ? searchTerm : ''} reviewList={searchResult.reviews} />
      </div>
    );
  }

  return (
    <>
      <h1> Search My Tech Review</h1>
      <Search to="/#/search" search={searchTerm !== null ? searchTerm : ''} />
      <br />

      {productDiv == null && brandDiv == null && reviewDiv == null && <p> No results found. </p>}
      {productDiv}
      {brandDiv}
      {reviewDiv}
    </>
  );
};

export default GeneralSearch;
