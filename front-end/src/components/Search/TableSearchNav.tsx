import React from 'react';
import Nav from 'react-bootstrap/Nav';

import './SearchStyle.css';
type NavProps = {
  activeKey: string;
  url: string;
};

const TableSearchNav = ({ activeKey, url }: NavProps): JSX.Element => {
  return (
    <Nav variant="pills" activeKey={activeKey} className="search-tab">
      <Nav.Item>
        <Nav.Link href={url}>View All</Nav.Link>
      </Nav.Item>
      <Nav.Item id={'search-nav'}>
        <Nav.Link href={url + '-search'} id={'search-nav-link'}>
          Search
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
};

export default TableSearchNav;
