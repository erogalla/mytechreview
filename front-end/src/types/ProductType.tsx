type ProductType = {
  id: number;
  name: string;
  brandId: number;
  price: number;
  // releaseDate: string;
  // numbersSold: number;
  rating: number;
  // cpu: string;
  memory: number;
  screen: string;
  os: string;
  batteryLife: number;
  // logo: ImageType;
  numReviews: number;
  image: string;
  ram: string;
  url: string;
};

export default ProductType;
