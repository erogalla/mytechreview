export type GitlabStatsType = {
  name: string;
  email: string;
  username: string;
  img: string;
  role: string;
  bio: string;
  commits: number;
  issues: number;
  tests: number;
};

export type GitlabTestsType = {
  numTests: number;
};

export type CommitsApiCallType = {
  email: string;
  commits: number;
};

export type IssuesApiCallType = {
  assignee: {
    username: string;
  };
};

export type PipelineApiCallType = {
  id: number;
  project_id: number;
  sha: string;
  ref: string;
  status: string;
  source: string;
};

export type TestReportApiCallType = {
  total_time: number;
  total_count: number;
  success_count: number;
  failed_count: number;
};
