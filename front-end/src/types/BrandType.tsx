type BrandType = {
  id: number;
  name: string;
  description: string;
  img: string;
  avgRating: number;
  numProducts: number;
  nameOfCEO: string;
  founded: number;
  country: string;
  // numCustomers: number;
  // numProductsSold: number;
  revenue: number;
  numRatings: number;
  num_employees: number;
};

export default BrandType;
