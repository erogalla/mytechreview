type ReviewType = {
  id: number;
  title: string;
  username: string;
  description: string;
  product: string;
  numStars: number;
  submissionTime: string;
  productId: number;
  numHelpful: number;
  numUnhelpful: number;
  numValue: number;
  numQuality: number;
  numEaseOfUse: number;
  prodBrand: string;
  brandId: number;
};

export default ReviewType;
