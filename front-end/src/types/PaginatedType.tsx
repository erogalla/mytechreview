import ProductType from './ProductType';
import BrandType from './BrandType';
import ReviewType from './ReviewType';

export type PaginatedProductType = {
  current: number;
  next: number;
  prev: number;
  pages: number;
  data: ProductType[];
};

export type PaginatedBrandType = {
  current: number;
  next: number;
  prev: number;
  pages: number;
  data: BrandType[];
};

export type PaginatedReviewType = {
  current: number;
  next: number;
  prev: number;
  pages: number;
  data: ReviewType[];
};
