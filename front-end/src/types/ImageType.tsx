type ImageType = {
  href: string;
  description: string;
};

export default ImageType;
