export type AboutInfoType = {
  title: string;
  description: string;
  img: string;
  link: string;
};
