// Functions
import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

// Componenets
import {
  SplashRoute,
  AboutRoute,
  BrandRoute,
  BrandsTableRoute,
  BrandsSearchRoute,
  ProductRoute,
  ProductsTableRoute,
  ProductsSearchRoute,
  ReviewRoute,
  ReviewsTableRoute,
  ReviewsSearchRoute,
  SearchRoute,
  VisualizationsRoute,
  ProviderVisualizationsRoute,
} from './routes';

const App = (): JSX.Element => {
  return (
    <HashRouter>
      <Switch>
        <Route exact path="/" component={SplashRoute} />
        <Route path="/about/" component={AboutRoute} />
        <Route path="/brands/" component={BrandsTableRoute} />
        <Route path="/brands-search" component={BrandsSearchRoute} />
        <Route path="/brand/:brandID" component={BrandRoute} />
        <Route path="/products/" component={ProductsTableRoute} />
        <Route path="/products-search" component={ProductsSearchRoute} />
        <Route path="/product/:productId" component={ProductRoute} />
        <Route path="/reviews/" component={ReviewsTableRoute} />
        <Route path="/review/:reviewId" component={ReviewRoute} />
        <Route path="/reviews-search" component={ReviewsSearchRoute} />
        <Route path="/search" component={SearchRoute} />
        <Route path="/visualizations" component={VisualizationsRoute} />
        <Route path="/provider-visualizations" component={ProviderVisualizationsRoute} />
      </Switch>
    </HashRouter>
  );
};
export default App;
