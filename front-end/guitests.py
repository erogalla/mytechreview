import sys, os
from sys import platform

# TO RUN:
# python ./front-end/guitests.py

# Inspired by TexasVotes: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/guitests.py

if __name__ == "__main__":
  try:
    python = sys.argv[1]
  except:
    python = 'python3'

  if platform == "win32":
      PATH = "./selenium_tests/chromedriver.exe"
  elif platform == "linux":
      PATH = "./selenium_tests/chromedriver_linux"
  else:
      print("Unsupported OS")
      exit(-1)

  # PATH = "./selenium_tests/chromedriver"

  # Run tests
  os.system(python + " ./selenium_tests/SplashTests.py " + PATH)
  os.system(python + " ./selenium_tests/ProductsTests.py " + PATH)
  os.system(python + " ./selenium_tests/ReviewsTests.py " + PATH)
  os.system(python + " ./selenium_tests/BrandsTests.py " + PATH)
  os.system(python + " ./selenium_tests/AboutTests.py " + PATH)
