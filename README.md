## Name: Gitlab ID, EID
Daniel Cai: @danielhcai, dhc674
Emilio Rogalla: @erogalla, eer2237
Md Safin al Wasi: @SafinWasi, mw37673
Sarah Zhang: @sarahz00, sz6753
Shizuka Bai: @Shizuka0000, sb55983

## Project Leader
Phase I: Sarah Zhang
Phase II: Md Safin al Wasi
Phase III: Emilio Rogalla
Phase IV: Daniel Cai

## Link to Website
https://www.mytechreview.me/

## Link to the Presentation Video
https://youtu.be/UHwOVKghWs4

## Gitlab Pipelines
https://gitlab.com/erogalla/mytechreview/-/pipelines

## GIT SHA
Phase I: 074e16f24d02fec01fcecf2ea9f2b4f33451ea9e
Phase II: 01ebb8f93c03ca4f1b08e09b53dc18dc429a8680
Phase III: 939ab95ecd5c51f57571355cac3572522320010e
Phase IV: 43be340c8cda087c3f566feaa372fd36c6a57c46

## Estimated Completion Time
- Phase I:
  Daniel Cai: 10
  Emilio Rogalla: 10
  Md Safin al Wasi: 10
  Sarah Zhang: 10
  Shizuka Bai: 10

- Phase II:
  Daniel Cai: 20
  Emilio Rogalla: 20
  Md Safin al Wasi: 17
  Sarah Zhang: 15
  Shizuka Bai: 20

- Phase III:
  Daniel Cai: 15
  Emilio Rogalla: 15
  Md Safin al Wasi: 12
  Sarah Zhang: 15
  Shizuka Bai: 13

- Phase IV:
  Daniel Cai: 5
  Emilio Rogalla: 5
  Md Safin al Wasi: 2
  Sarah Zhang: 8
  Shizuka Bai: 3

  ## Actual Completion Time
- Phase I:
  Daniel Cai: 12
  Emilio Rogalla: 17
  Md Safin al Wasi: 13
  Sarah Zhang: 13
  Shizuka Bai: 12

- Phase II:
  Daniel Cai: 23
  Emilio Rogalla: 23
  Md Safin al Wasi: 28
  Sarah Zhang: 20
  Shizuka Bai: 23

- Phase III:
  Daniel Cai: 20
  Emilio Rogalla: 15
  Md Safin al Wasi: 16
  Sarah Zhang: 20
  Shizuka Bai: 13

- Phase IV:
  Daniel Cai: 5
  Emilio Rogalla: 7
  Md Safin al Wasi: 2
  Sarah Zhang: 10
  Shizuka Bai: 4

## Comment

