"""
Python unit tests for the API
"""

import unittest
import requests
import os
import app
app.app.testing = True

# pylint: disable = missing-docstring

URL = "http://127.0.0.1:5000/"

FAILURE = b'{"error":"-1 ID not found"}\n'


class TestApiRequests(unittest.TestCase):
    def test_get_all_products(self):
#        all_products = requests.get(URL + "products")
        with app.app.test_client() as c:
            all_products = c.get('/products')
        self.assertEqual(all_products.status_code, 200)
        self.assertIsInstance(all_products.json, list)
        self.assertEqual(len(all_products.json), 1006)

    def test_get_single_product(self):
        with app.app.test_client() as c:
            single_product = c.get('/product/id=0')
        #single_product = requests.get(URL + "product/id=1")
        self.assertEqual(single_product.status_code, 200)
        targetResponse = {
            "id": 0,
            "name": "Apple - iPhone 11 128GB - Black (AT&T)",
            "brand": "Apple",
            "brandId": 10,
            "price": 649.99,
            "memory": "128",
            "ram": None,
            "os": "Apple iOS 13",
            "battery": None,
            "screen": "6",
            "url": "https://api.bestbuy.com/click/-/6341383/pdp",
            "image": "https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6341/6341383_sa.jpg",
            "rating": 5,
        }
        self.assertIsInstance(single_product.json, dict)
        self.assertDictEqual(single_product.json, targetResponse)

    def test_single_product_fail(self):
        with app.app.test_client() as c:
            single_product_fail = c.get('/product/id=65535')
        self.assertEqual(single_product_fail.status_code, 404)
        self.assertEqual(single_product_fail.data, FAILURE)

    def test_get_all_brands(self):
        #all_brands = requests.get(URL + "brands")
        with app.app.test_client() as c:
            all_brands = c.get('/brands')
        self.assertEqual(all_brands.status_code, 200)
        self.assertIsInstance(all_brands.json, list)
        self.assertEqual(len(all_brands.json), 123)

    def test_get_single_brand(self):
        #single_brand = requests.get(URL + "brand/id=4")
        with app.app.test_client() as c:
            single_brand = c.get('/brand/id=4')
        self.assertEqual(single_brand.status_code, 200)
        self.assertIsInstance(single_brand.json, dict)
        targetResponse = {
            "description": "ASUSTek Computer Inc. is a Taiwanese multinational computer and phone hardware and electronics company headquartered in Beitou District, Taipei, Taiwan. ",
            "founded": 1989,
            "id": 4,
            "img": "https://upload.wikimedia.org/wikipedia/commons/8/81/AsusTek_logo.svg",
            "name": "ASUS",
            "numProducts": 28,
            "numRatings": 349,
            "num_employees": "5,892",
            "revenue": 351.33,
            "url": None,
            "wikiUrl": "https://en.wikipedia.org/wiki/Asus",
            "country": "Taiwan",
            "avgRating": 4,
            "numRatings": 349,
        }
        self.assertDictEqual(single_brand.json, targetResponse)

    def test_get_single_brand_fail(self):
        # single_brand_fail = requests.get(URL + "brand/id=45667")
        with app.app.test_client() as c:
            single_brand_fail = c.get('/brand/id=45667')
        self.assertEqual(single_brand_fail.status_code, 404)
        self.assertEqual(single_brand_fail.data, FAILURE)

    def test_get_all_reviews(self):
        # all_reviews = requests.get(URL + "reviews")
        with app.app.test_client() as c:
            all_reviews = c.get('/reviews')
        self.assertEqual(all_reviews.status_code, 200)
        self.assertIsInstance(all_reviews.json, list)
        self.assertEqual(len(all_reviews.json), 9896)

    def test_get_single_review(self):
        # single_review = requests.get(URL + "review/id=5")
        with app.app.test_client() as c:
            single_review = c.get('/review/id=5')
        self.assertEqual(single_review.status_code, 200)
        self.assertIsInstance(single_review.json, dict)
        targetResponse = {
            "username": "georgecamera",
            "title": "new i-phone",
            "description": "great product love the new att i phone good sales people to work with knew the product",
            "numStars": 5,
            "numHelpful": 1,
            "numUnhelpful": 0,
            "numValue": -1,
            "numEaseOfUse": -1,
            "numQuality": -1,
            "productId": 0,
            "brandId" : 10,
            "product" : "Apple - iPhone 11 128GB - Black (AT&T)",
            "prodBrand" : "Apple",
            "submissionTime": "2021-07-24T00:28:00.000Z",
            "product": "Apple - iPhone 11 128GB - Black (AT&T)",
            "id": 5,
            "brandId": 10,
            "prodBrand": "Apple",
        }
        self.assertDictEqual(single_review.json, targetResponse)

    def test_get_single_review_fail(self):
        with app.app.test_client() as c:
            single_review_fail = c.get('/review/id=45000')
        self.assertEqual(single_review_fail.status_code, 404)
        self.assertEqual(single_review_fail.data, FAILURE)


if __name__ == "__main__":
    if "CI_PROJECT_NAMESPACE" in os.environ:
        print("In Gitlab")
    else:
        print("In local")
    unittest.main()
