from itertools import product
from flask import Flask, json, jsonify, request
from flask_cors import CORS
import flask_sqlalchemy
import os
from dotenv import load_dotenv
from sqlalchemy import desc, asc, and_
import sys

load_dotenv()


app = Flask(__name__)
CORS(app)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
# print(os.getenv("AWS_DB_KEY"))
# app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
db = flask_sqlalchemy.SQLAlchemy(app)

# suppress warning
err = "-1 ID not found"

NUM_PER_PAGE = 20


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    brand = db.Column(db.Text)
    brandId = db.Column(db.Integer)
    price = db.Column(db.Float)
    memory = db.Column(db.Text)
    ram = db.Column(db.Text)
    os = db.Column(db.Text)
    battery = db.Column(db.Text)
    screen = db.Column(db.Text)
    url = db.Column(db.Text)
    image = db.Column(db.Text)
    rating = db.Column(db.Integer)

    def __repr__(self):
        return "<" + str(self.id) + ":%r>" % self.name


class Brand(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    numProducts = db.Column(db.Integer)
    founded = db.Column(db.Integer)
    revenue = db.Column(db.Float)
    num_employees = db.Column(db.Text)
    url = db.Column(db.Text)
    description = db.Column(db.Text)
    wikiUrl = db.Column(db.Text)
    img = db.Column(db.Text)
    country = db.Column(db.Text)
    avgRating = db.Column(db.Integer)
    numRatings = db.Column(db.Integer)

    def __repr__(self):
        return "<Brand name %r>" % self.name


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text)
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    numStars = db.Column(db.Integer)
    numHelpful = db.Column(db.Integer)
    numUnhelpful = db.Column(db.Integer)
    numValue = db.Column(db.Integer)
    numEaseOfUse = db.Column(db.Integer)
    numQuality = db.Column(db.Integer)
    productId = db.Column(db.Integer)
    brandId = db.Column(db.Integer)
    product = db.Column(db.Text)
    prodBrand = db.Column(db.Text)
    submissionTime = db.Column(db.Text)

    def __repr__(self):
        return "<Reviewer: %r>" % self.username


db.create_all()


def as_dict(source):
    return {c.name: getattr(source, c.name) for c in source.__table__.columns}


def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


def makePages(pagination):
    items = pagination.items
    dataList = [as_dict(item) for item in items]
    output = {}
    output["pages"] = pagination.pages
    output["current"] = pagination.page
    output["next"] = pagination.next_num
    output["prev"] = pagination.prev_num
    output["data"] = dataList
    return output


@app.route("/")
def help():
    output = {}
    helpSection = {}
    helpSection["Products"] = "/products"
    helpSection["Brands"] = "/brands"
    helpSection["Reviews"] = "/reviews"
    helpSection["sorting"] = "/<endpoint>?action=sort&attribute=<attribute>"
    helpSection["filtering"] = "/<endpoint>?action=filter&<**attributes>"
    helpSection[
        "descending_sort"
    ] = "/<endpoint>?action=sort&attribute=<attribute>&desc=true"
    helpSection["pagination"] = "/<endpoint>?paginate=true&page=<1-n>"
    examples = {}
    examples["sorting"] = "/brands?action=sort&attribute=numProducts&desc=true"
    examples["filtering"] = "/products?action=filter&brand=Apple&price=649.99"
    examples["pagination"] = "/products?action=filter&brand=Apple&paginate=true&page=3"
    output["help"] = helpSection
    output["examples"] = examples
    return jsonify(output)


@app.route("/products")
def get_products():
    paginate = request.args.get("paginate")
    queries = request.args.to_dict(flat=False)
    prod_query = db.session.query(Product)
    page = request.args.get("page")
    des = request.args.get("desc")
    sort = request.args.get("sort")
    products = Product.query
    try:
        products = filter_products(prod_query, queries)
    except Exception as e:
        print(e)
        response = {"error": err}
        return jsonify(response), 404
    if sort is not None:
        try:
            if des is not None:
                products = products.order_by(desc(getattr(Product, sort)))
            else:
                products = products.order_by(asc(getattr(Product, sort)))
        except Exception as e:
            print(e)
            response = {"error": err}
            return jsonify(response), 404
    if paginate is None:
        products = products.all()
    else:
        if page is None:
            pageNum = 1
        else:
            try:
                pageNum = int(page)
            except ValueError:
                response = {"error": err}
                return jsonify(response), 404
        pagination = products.paginate(pageNum, NUM_PER_PAGE, error_out=True)
        result = makePages(pagination)
        return jsonify(result)
    productList = [as_dict(product) for product in products]

    return jsonify(productList)


@app.route("/products/page=<int:page>")
def get_products_page(page):
    pagination = Product.query.paginate(page, NUM_PER_PAGE, error_out=True)
    products = pagination.items
    productList = [as_dict(product) for product in products]
    output = {}
    output["pages"] = pagination.pages
    output["next"] = pagination.next_num
    output["prev"] = pagination.prev_num
    output["data"] = productList
    return jsonify(output)


@app.route("/product/id=<int:product_id>")
def get_product(product_id):
    product = Product.query.filter_by(id=product_id).first()
    if product == None:
        response = {"error": err}
        return jsonify(response), 404
    return jsonify(as_dict(product))


@app.route("/product/brand_id=<int:brand_id>")
def get_product_by_brand(brand_id):
    products = Product.query.filter_by(brandId=brand_id).all()
    productList = [as_dict(product) for product in products]
    return jsonify(productList)


@app.route("/products/search")
def get_products_search():
    search = request.args.get("search")
    page = request.args.get("page")
    if search == None or page == None:
        return jsonify({"error": err}), 404
    try:
        page = int(page)
    except ValueError:
        return jsonify({"error": err}), 404

    if search == "":
        return jsonify([])

    search_terms = [term.lower() for term in search.split()]
    search_queries = []
    for term in search_terms:
        like_term = "%" + term + "%"

        name_q = Product.query.filter(Product.name.ilike(like_term))
        brand_q = Product.query.filter(Product.brand.ilike(like_term))
        price_q = Product.query.filter(term in str(Product.price))
        memory_q = Product.query.filter(Product.memory.ilike(like_term))

        search_queries.append(name_q.union(brand_q, price_q, memory_q))

    final_query = search_queries[0]
    for query in search_queries[1:]:
        final_query = final_query.union(query)
    count = final_query.count()
    pagination = final_query.paginate(page, NUM_PER_PAGE, error_out=True)
    reviews = pagination.items
    brandList = [as_dict(review) for review in reviews]
    output = {"data": brandList, "totalCount": count}
    return jsonify(output)


@app.route("/product/budget=<int:budget>")
def get_product_by_budget(budget):
    products = Product.query.filter(Product.price <= budget).all()
    productList = [as_dict(product) for product in products]
    return jsonify(productList)


@app.route("/brands")
def get_brands():
    paginate = request.args.get("paginate")
    page = request.args.get("page")
    sort = request.args.get("sort")
    des = request.args.get("desc")
    queries = request.args.to_dict(flat=False)
    brand_query = db.session.query(Brand)
    brands = Brand.query
    try:
        brands = filter_brands(brand_query, queries)
    except Exception as e:
        print(e)
        response = {"error": err}
        return jsonify(response), 404
    if sort is not None:
        print(sort)
        try:
            if des == "True" or des == "true":
                brands = brands.order_by(desc(getattr(Brand, sort)))
            else:
                brands = brands.order_by(asc(getattr(Brand, sort)))
        except Exception as e:
            print(e)
            response = {"error": err}
            return jsonify(response), 404
    if paginate is None:
        brands = brands.all()
    else:
        if page is None:
            pageNum = 1
        else:
            try:
                pageNum = int(page)
            except ValueError:
                response = {"error": err}
                return jsonify(response), 404
        pagination = brands.paginate(pageNum, NUM_PER_PAGE, error_out=True)
        result = makePages(pagination)
        return jsonify(result)

    brandList = [as_dict(brand) for brand in brands]
    response = jsonify(brandList)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/brands/page=<int:page>")
def get_brands_page(page):
    pagination = Brand.query.paginate(page, NUM_PER_PAGE, error_out=True)
    brands = pagination.items
    brandList = [as_dict(brand) for brand in brands]
    output = {}
    output["pages"] = pagination.pages
    output["next"] = pagination.next_num
    output["prev"] = pagination.prev_num
    output["data"] = brandList
    return jsonify(output)


@app.route("/brands/search")
def get_brands_search():
    search = request.args.get("search")
    page = request.args.get("page")
    if search == None or page == None:
        return jsonify({"error": err}), 404
    try:
        page = int(page)
    except ValueError:
        return jsonify({"error": err}), 404

    if search == "":
        return jsonify([])

    search_terms = [term.lower() for term in search.split()]
    search_queries = []
    for term in search_terms:
        like_term = "%" + term + "%"
        name_q = Brand.query.filter(Brand.name.ilike(like_term))
        desc_q = Brand.query.filter(Brand.description.ilike(like_term))
        country_q = Brand.query.filter(Brand.country.ilike(like_term))
        founded_q = Brand.query.filter(term in str(Brand.founded))
        search_queries.append(name_q.union(desc_q, country_q, founded_q))

    final_query = search_queries[0]
    for query in search_queries[1:]:
        final_query = final_query.union(query)
    count = final_query.count()
    pagination = final_query.paginate(page, NUM_PER_PAGE, error_out=True)
    brands = pagination.items
    brandList = [as_dict(brand) for brand in brands]
    output = {"data": brandList, "totalCount": count}
    return jsonify(output)


@app.route("/brand/id=<int:brand_id>")
def get_brand(brand_id):
    brand = Brand.query.filter_by(id=brand_id).first()
    if brand == None:
        response = {"error": err}
        return jsonify(response), 404
    return jsonify(as_dict(brand))


@app.route("/reviews")
def get_reviews():
    sort = request.args.get("sort")
    paginate = request.args.get("paginate")
    page = request.args.get("page")
    des = request.args.get("desc")
    queries = request.args.to_dict(flat=False)
    review_query = db.session.query(Review)
    reviews = Review.query
    try:
        reviews = filter_reviews(review_query, queries)
    except Exception as e:
        print(e)
        response = {"error": err}
        return jsonify(response), 404
    if sort is not None:
        try:
            if des == "True" or des == "true":
                reviews = reviews.order_by(desc(getattr(Review, sort)))
            else:
                reviews = reviews.order_by(asc(getattr(Review, sort)))
        except Exception as e:
            print(e)
            response = {"error": err}
            return jsonify(response), 404

    if paginate is None:
        reviews = reviews.all()
    else:
        if page is None:
            pageNum = 1
        else:
            try:
                pageNum = int(page)
            except ValueError:
                response = {"error": err}
                return jsonify(response), 404
        pagination = reviews.paginate(pageNum, NUM_PER_PAGE, error_out=True)
        result = makePages(pagination)
        return jsonify(result)

    reviewList = [as_dict(review) for review in reviews]
    return jsonify(reviewList)


@app.route("/reviews/page=<int:page>")
def get_reviews_page(page):
    pagination = (
        db.session.query(Review, Product)
        .filter(Review.productId == Product.id)
        .paginate(page, NUM_PER_PAGE, error_out=True)
    )
    reviews = pagination.items
    reviewList = []
    for review, product in reviews:
        review_dict = as_dict(review)
        review_dict["productName"] = product.name
        reviewList.append(review_dict)
    output = {}
    output["pages"] = pagination.pages
    output["next"] = pagination.next_num
    output["prev"] = pagination.prev_num
    output["data"] = reviewList
    return jsonify(output)


@app.route("/review/id=<int:review_id>")
def get_review(review_id):
    review = Review.query.filter_by(id=review_id).first()
    product = Product.query.filter_by(id=(Review.productId + 1)).first()
    if review == None:
        response = {"error": err}
        return jsonify(response), 404
    response = as_dict(review)
    return jsonify(response)


@app.route("/review/product_id=<int:product_id>")
def get_review_by_product(product_id):
    reviews = Review.query.filter_by(productId=product_id).all()
    reviewList = [as_dict(review) for review in reviews]
    return jsonify(reviewList)


@app.route("/review/brand_id=<int:brand_id>")
def get_review_by_brand(brand_id):
    reviews = (
        db.session.query(Review, Product)
        .filter(Review.productId == Product.id)
        .filter(Product.brandId == brand_id)
        .all()
    )
    reviewList = []
    for review, product in reviews:
        review_dict = as_dict(review)
        review_dict["productName"] = product.name
        reviewList.append(review_dict)
    return jsonify(reviewList)


# Model filtering is heavily inspired by
# https://gitlab.com/forbesye/fitsbits/


def filter_products(prod_query, queries):
    brand = get_query("brand", queries)
    rating = get_query("rating", queries)
    operating = get_query("os", queries)
    priceRange = get_query("priceRange", queries)
    screen = get_query("screen", queries)
    ram = get_query("ram", queries)

    if brand != None:
        prod_query = filter_products_by(prod_query, "brand", brand)

    if rating != None:
        prod_query = filter_products_by(prod_query, "rating", rating)

    if operating != None:
        prod_query = filter_products_by(prod_query, "os", operating)

    if priceRange != None:
        priceRange = priceRange[0]
        min_price = 0
        max_price = sys.float_info.max
        if len(priceRange.split("-")) < 2:
            min_price = int(priceRange)
        else:
            min_price, max_price = priceRange.split("-")
        prod_query = filter_products_by(
            prod_query, "priceRange", [min_price, max_price]
        )

    if screen != None:
        prod_query = filter_products_by(prod_query, "screen", screen)
    
    if ram != None:
        prod_query = filter_products_by(prod_query, "ram", ram)

    return prod_query


def filter_products_by(prod_query, filtering, what):
    if filtering == "brand":
        prod_query = prod_query.filter(Product.brand.in_(what))

    elif filtering == "rating":
        # pass
        prod_query = prod_query.filter(Product.rating.in_(what))

    elif filtering == "os":
        prod_query = prod_query.filter(Product.os.in_(what))

    elif filtering == "priceRange":
        prod_query = prod_query.filter(
            and_(
                Product.price >= what[0],
                Product.price <= what[1],
            )
        )

    elif filtering == "screen":
        prod_query = prod_query.filter(Product.screen.in_(what))

    elif filtering == "ram":
        prod_query = prod_query.filter(Product.ram.in_(what))

    return prod_query


def filter_brands(brand_query, queries):
    # country, avgRating, founded, productNumRange, reviewRange
    country = get_query("country", queries)
    avgRating = get_query("avgRating", queries)
    founded = get_query("founded", queries)
    productNumRange = get_query("numProducts", queries)
    reviewNumRange = get_query("numRatings", queries)

    if country != None:
        brand_query = filter_brands_by(brand_query, "country", country)

    if avgRating != None:
        brand_query = filter_brands_by(brand_query, "avgRating", avgRating)

    if founded != None:
        brand_query = filter_brands_by(brand_query, "founded", founded)

    if productNumRange != None:
        productNumRange = productNumRange[0]
        min_prod = 0
        max_prod = 65535
        if len(productNumRange.split("-")) < 2:
            min_prod = int(productNumRange)
        else:
            min_prod, max_prod = productNumRange.split("-")
        brand_query = filter_brands_by(
            brand_query, "productNumRange", [min_prod, max_prod]
        )

    if reviewNumRange != None:
        reviewNumRange = reviewNumRange[0]
        min_rev = 0
        max_rev = 65535
        if len(reviewNumRange.split("-")) < 2:
            min_rev = int(reviewNumRange)
        else:
            min_rev, max_rev = reviewNumRange.split("-")
        brand_query = filter_brands_by(
            brand_query, "reviewNumRange", [min_rev, max_rev]
        )
    return brand_query


def filter_brands_by(brand_query, filtering, what):

    if filtering == "country":
        brand_query = brand_query.filter(Brand.country.in_(what))
    elif filtering == "avgRating":
        brand_query = brand_query.filter(Brand.avgRating.in_(what))
    elif filtering == "founded":
        brand_query = brand_query.filter(Brand.founded.in_(what))
    elif filtering == "productNumRange":
        brand_query = brand_query.filter(
            and_(
                Brand.numProducts >= what[0],
                Brand.numProducts <= what[1],
            )
        )
    elif filtering == "reviewNumRange":
        brand_query = brand_query.filter(
            and_(
                Brand.numRatings >= what[0],
                Brand.numRatings <= what[1],
            )
        )
    return brand_query


def filter_reviews(review_query, queries):
    # product brand, rating, product, number of helpful, numUnHelpful
    prodBrand = get_query("prodBrand", queries)
    rating = get_query("rating", queries)
    product = get_query("product", queries)
    numHelpful = get_query("numHelpful", queries)
    numUnHelpful = get_query("numUnHelpful", queries)

    if prodBrand != None:
        review_query = filter_reviews_by(review_query, "prodBrand", prodBrand)

    if rating != None:
        review_query = filter_reviews_by(review_query, "rating", rating)

    if product != None:
        review_query = filter_reviews_by(review_query, "product", product)

    if numHelpful != None:
        review_query = filter_reviews_by(review_query, "helpful", numHelpful)

    if numUnHelpful != None:
        review_query = filter_reviews_by(review_query, "unHelpful", numUnHelpful)

    return review_query


def filter_reviews_by(review_query, filtering, what):
    if filtering == "prodBrand":
        review_query = review_query.filter(Review.prodBrand.in_(what))
    elif filtering == "rating":
        review_query = review_query.filter(Review.numStars.in_(what))

    elif filtering == "product":
        review_query = review_query.filter(Review.product.in_(what))

    elif filtering == "helpful":
        review_query = review_query.filter(Review.numHelpful.in_(what))

    elif filtering == "unHelpful":
        review_query = review_query.filter(Review.numUnhelpful.in_(what))

    return review_query


@app.route("/reviews/search")
def get_reviews_search():
    search = request.args.get("search")
    page = request.args.get("page")
    if search == None or page == None:
        return jsonify({"error": err}), 404
    try:
        page = int(page)
    except ValueError:
        return jsonify({"error": err}), 404

    if search == "":
        return jsonify([])

    search_terms = [term.lower() for term in search.split()]
    search_queries = []
    for term in search_terms:
        like_term = "%" + term + "%"

        name_q = Review.query.filter(Review.username.ilike(like_term))
        title_q = Review.query.filter(Review.title.ilike(like_term))
        desc_q = Review.query.filter(Review.description.ilike(like_term))
        star_q = Review.query.filter(str(Review.numStars) == term)
        search_queries.append(name_q.union(title_q, desc_q, star_q))

    final_query = search_queries[0]
    for query in search_queries[1:]:
        final_query = final_query.union(query)
    count = final_query.count()
    pagination = final_query.paginate(page, NUM_PER_PAGE, error_out=True)
    reviews = pagination.items
    brandList = [as_dict(review) for review in reviews]
    output = {"data": brandList, "totalCount": count}
    return jsonify(output)


@app.route("/search")
def get_search():
    search = request.args.get("search")
    if search == None:
        return jsonify({"error": err}), 404

    if search == "":
        return jsonify(
            {
                "products": [],
                "numProducts": 0,
                "brands": [],
                "numBrands": 0,
                "reviews": [],
                "numReviews": 0,
            }
        )

    search_terms = [term.lower() for term in search.split()]
    product_query = []
    brand_query = []
    review_query = []

    for term in search_terms:
        like_term = "%" + term + "%"

        name_q = Product.query.filter(Product.name.ilike(like_term))
        brand_q = Product.query.filter(Product.brand.ilike(like_term))
        price_q = Product.query.filter(term in str(Product.price))
        memory_q = Product.query.filter(Product.memory.ilike(like_term))
        product_query.append(name_q.union(brand_q, price_q, memory_q))

        name_q = Review.query.filter(Review.username.ilike(like_term))
        title_q = Review.query.filter(Review.title.ilike(like_term))
        desc_q = Review.query.filter(Review.description.ilike(like_term))
        star_q = Review.query.filter(str(Review.numStars) == term)
        review_query.append(name_q.union(title_q, desc_q, star_q))

        name_q = Brand.query.filter(Brand.name.ilike(like_term))
        desc_q = Brand.query.filter(Brand.description.ilike(like_term))
        country_q = Brand.query.filter(Brand.country.ilike(like_term))
        founded_q = Brand.query.filter(term in str(Brand.founded))
        brand_query.append(name_q.union(desc_q, country_q, founded_q))

    output = {}
    reviews = review_query[0]
    for query in review_query[1:]:
        reviews = reviews.union(query)
    output["reviews"] = [as_dict(r) for r in reviews.limit(9)]
    output["numReviews"] = reviews.count()

    products = product_query[0]
    for query in product_query[1:]:
        products = products.union(query)
    output["products"] = [as_dict(p) for p in products.limit(9)]
    output["numProducts"] = products.count()

    brands = brand_query[0]
    for query in brand_query[1:]:
        brands = brands.union(query)
    output["brands"] = [as_dict(b) for b in brands.limit(9)]
    output["numBrands"] = brands.count()

    return jsonify(output)


@app.route("/review/username=<string:username>")
def get_review_by_username(username):
    reviews = Review.query.filter_by(username=username).all()
    reviewList = [as_dict(review) for review in reviews]
    return jsonify(reviewList)
